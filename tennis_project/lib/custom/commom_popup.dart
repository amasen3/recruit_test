import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/material.dart';

class CommonPopup extends StatelessWidget {
  BuildContext context;
  String optionType;
  List<String> param;
  Function deleteItem;
  CommonPopup(this.context, this.optionType, this.param, this.deleteItem);

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;

  var btnWidth = 100.0;
  var btnFont = 20.0;

  var paddingData = const EdgeInsets.only(right: 50.0, bottom: 10.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      btnWidth = 80.0;
      btnFont = 17;

      paddingData = const EdgeInsets.only(right: 20.0, bottom: 10.0);
    } else if (deviceWidth <= 400) {
      btnWidth = 80.0;
      btnFont = 17;

      paddingData = const EdgeInsets.only(right: 20.0, bottom: 10.0);
    } else if (deviceWidth <= 450) {
      btnWidth = 80.0;
      btnFont = 17;

      paddingData = const EdgeInsets.only(right: 20.0, bottom: 10.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(right: 50.0, bottom: 10.0);
    } else if (deviceWidth <= 550) {

      paddingData = const EdgeInsets.only(right: 30.0, bottom: 10.0);
    } else if (deviceWidth > 550) {
      paddingData = const EdgeInsets.only(right: 25.0, bottom: 10.0);
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    deviceWidth = MediaQuery.of(context).size.width;
    deviceHeight = MediaQuery.of(context).size.height;

    _setDeviceWidth();

    var textDesplay = "";
    var iconDesplay;

    if (optionType == "delete") {
      textDesplay = "Delete";
      iconDesplay = Icons.delete_forever;
    }
    if (optionType == "edit") {
      textDesplay = "Update";
      iconDesplay = Icons.update;
    }

    return MediaQuery(
      data: MediaQueryData(),
      child: AlertDialog(
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Icon(iconDesplay, size: 30, color: Colors.black),
                ],
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: new Text(
                      textDesplay,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                          color: Colors.black,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        content:  Container(
          child: new Text(
                "This content will be " +
                    textDesplay +
                    ". Do you want to continue ?",
                textAlign: TextAlign.justify,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    color: Colors.black,
                    fontStyle: FontStyle.normal)),
        ),
        
        actions: <Widget>[
          Padding(
            padding: paddingData,
            child: Container(
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: btnWidth,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              side: BorderSide(
                                  color: AppStyle.color_Head,
                                  style: BorderStyle.solid)),
                          color: AppStyle.color_Head,
                          child: new Text(
                            textDesplay,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: btnFont,
                                color: Colors.white,
                                fontStyle: FontStyle.normal),
                          ),
                          onPressed: () =>
                              {Navigator.of(context).pop(), deleteItem(param)},
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: SizedBox(
                          width: btnWidth,
                          child: new RaisedButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0),
                                side: BorderSide(
                                    color: AppStyle.color_Head,
                                    style: BorderStyle.solid)),
                            color: AppStyle.color_Head,
                            child: new Text(
                              'CANCEL',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: btnFont,
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
