import 'dart:ui';
import 'dart:io';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tenizo/tennizo_base_controller.dart';

class PlayerView extends StatefulWidget {
  final arguments;
  final Function onSelected;
  @override
  _PlayerViewState createState() => _PlayerViewState();

  PlayerView({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _PlayerViewState extends State<PlayerView> with BaseControllerListner {
  String sqlData, objectDbData;
  BaseController controller;

  Future<File> imageFile;

  String name = "";
  bool _imageAvailability;

  //DISABLED BUTTON
  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;

  // VARIABLES TO ASSIGN DATA COMING
  static var plyNumber = "";
  static var plyName = "";
  static var plyGender = "";
  static var plyDateOfBirth = "";
  static var plyHandedness = "";
  static var plyPlayStyle = "";
  static var plyTeam = "";
  static var plyRole = "";
  static var plyImage;

  @override
  void initState() {
    super.initState();

    if (widget.arguments['pnumber'] < 10) {
      plyNumber = "0" + widget.arguments['pnumber'].toString();
    } else {
      plyNumber = widget.arguments['pnumber'].toString();
    }

    plyName = widget.arguments['pname'].toString();
    plyGender = widget.arguments['pgender'].toString();
    plyDateOfBirth = widget.arguments['pdateofbirth'].toString();
    plyHandedness = widget.arguments['phandedness'].toString();
    plyPlayStyle = widget.arguments['pplaystyle'].toString();
    plyTeam = widget.arguments['pteam'].toString();
    plyRole = widget.arguments['prole'].toString();
    plyImage = widget.arguments['pplayerimage'];

    controller = new BaseController(this);

    //Check image availaibilty
    if (plyImage != null) {
      setState(() {
        _imageAvailability = checkImageAvailability(File(plyImage));
      });
    } else {
      setState(() {
        _imageAvailability = false;
      });
    }
  }

  customFont(text) {
    return Text(text, textAlign: TextAlign.left, style: customFontStyle(),
    overflow: TextOverflow.ellipsis);
  }

  customFontStyle() {
    return TextStyle(
        fontSize: 18,
        color: Colors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  customFontTopic(text) {
    return Text(
      text,
      textAlign: TextAlign.left,
      style: TextStyle(
          fontSize: 18,
          color: Colors.black,
          fontWeight: FontWeight.w600,
          fontFamily: 'Rajdhani'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Scaffold(
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
//Background image ----------------------------------------------
                  Stack(
                    children: <Widget>[
//backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: Container(
                              width: double.infinity,
                              height: 200,
                              decoration: BoxDecoration(
                                color: AppColors.gray,
                                image: _imageAvailability
                                    ? DecorationImage(
                                        fit: BoxFit.cover,
                                        image: getImageProvider(File(plyImage)))
                                    : null,
                              ),
                            ),
                          ),
                        ),
                      ),
//Profile image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 35.0),
                          child: Container(
                            width: 130.0,
                            height: 130.0,
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: plyImage != null
                                    ? getImageProvider(File(plyImage))
                                    : new AssetImage("images/user.png"),
                              ),
                              border: Border.all(
                                color: Colors.black,
                                width: 3.0,
                              ),
                            ),
                          ),
                        ),
                      ),

//profile name----------------------------
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 175.0),
                          child: Container(
                            height: 30.0,
                            child: Text(
                              name,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: 16.0,
                  ),

                  // name ----------------------------------------------
                  new Container(
                    padding: EdgeInsets.only(bottom: 8.0),
                    width: 300.0,
                    child: customFontTopic('Name'),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      0.0,
                      0.0,
                      0.0,
                      8.0,
                    ),

                    //NAME DISPLAYING FIELD
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(plyNumber + " - " + plyName),
                    ),
                  ),

//Gender and birth date ----------------------------------------------
//Gender -------------------------------------------------------------
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(right: 20.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                      padding: EdgeInsets.only(bottom: 8.0),
                                      child: customFontTopic('Gender')),
                                  new Container(
                                    width: 110.0,
                                    height: 48,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: AppColors.form_border,
                                        style: BorderStyle.solid,
                                        width: 1.2,
                                      ),
                                    ),
                                    child: customFont(plyGender),
                                  ),
                                ],
                              ),
                            ),

                            // Date of birth--------------------------------

                            Expanded(
                              child: Container(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(bottom: 8.0),
                                      child: customFontTopic('Date of Birth'),
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      width: double.infinity,
                                      height: 48,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(4.0),
                                        border: Border.all(
                                          color: AppColors.form_border,
                                          style: BorderStyle.solid,
                                          width: 1.2,
                                        ),
                                      ),
                                      child: customFont(plyDateOfBirth),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                  //Handedness ------------------------------------------------------------

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(bottom: 8.0),
                                child: customFontTopic('Handedness'),
                              ),
                              Container(
                                // width: 300.0,
                                height: 48,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.0),
                                  border: Border.all(
                                    color: AppColors.form_border,
                                    style: BorderStyle.solid,
                                    width: 1.2,
                                  ),
                                ),
                                child: customFont(plyHandedness),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),

                  //Play Style ------------------------------------------------------------

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                        width: 300,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(bottom: 8.0),
                                  child: customFontTopic('Play Style'),
                                ),
                                Container(
                                  height: 48,
                                  // width: 200,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                        color: AppStyle.color_Txt_Input_Border),
                                  ),
                                  child: customFont(plyPlayStyle),
                                ),
                              ],
                            ),
                          ),
                        )),
                  ),

                  //Role ----------------------------------------------

                  Container(
                    padding: EdgeInsets.only(bottom: 8.0),
                    width: 300.0,
                    child: customFontTopic('Role'),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                    child: new Container(
                      height: 48,
                      width: 300.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border:
                            Border.all(color: AppStyle.color_Txt_Input_Border),
                      ),
                      child: customFont(plyRole),
                    ),
                  ),

                  //Team ----------------------------------------------
                  new Container(
                    padding: EdgeInsets.only(bottom: 8.0),
                    width: 300.0,
                    child: customFontTopic('Team'),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      0.0,
                      0.0,
                      0.0,
                      0.0,
                    ),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border:
                            Border.all(color: AppStyle.color_Txt_Input_Border),
                      ),
                      child: customFont(plyTeam),
                    ),
                  ),

                  //OK button-------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 200,
                      height: 48,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: AppColors.ternary_color,
                          onPressed: () {
                            widget.onSelected(RoutingData.Players, true, false);
                          },
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: Colors.black,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "OK",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  // if file not found set default
  ImageProvider getImageProvider(File f) {
    return f.existsSync() ? FileImage(f) : const AssetImage("images/user.png");
  }

  //check available for set background image
  bool checkImageAvailability(File f) {
    return f.existsSync() ? true : false;
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            if (response['response_data'].toString() != "null") {
              setState(() {
                if (response['response_data'].toString() != "[]") {
                  sqlData = response['response_data'].toString();
                } else {
                  sqlData = "null";
                }
              });
            }
          }
          break;
        }
      default:
        {
          //Do nothing
        }
    }
  }
}
