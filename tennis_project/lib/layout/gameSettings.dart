import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:tenizo/custom/text_field.dart';
import 'package:tenizo/custom/drop_down.dart';
import 'package:tenizo/validation/textBoxValidation.dart';
import 'package:uuid/uuid.dart';

class GameSettings extends StatefulWidget {
  final Function onSelected;
  final Function onScoreArgumentSaving;
  final Function toggleStartState;
  final Function funcSetGameStart;
  final Function setTempPlayerScores;
  final String userName;
  @override
  _GameSettings createState() => _GameSettings();

  GameSettings(
      {Key key,
      this.onSelected,
      this.funcSetGameStart,
      this.onScoreArgumentSaving,
      this.toggleStartState,
      this.userName,
      this.setTempPlayerScores})
      : super(key: key);
}

class _GameSettings extends State<GameSettings> with BaseControllerListner {
  BaseController controller;
  FocusNode _focusPlayerA = new FocusNode();
  FocusNode _focusPlayerB = new FocusNode();
  FocusNode _focusMatchName = new FocusNode();
  FocusNode _focusNumberOfGames = new FocusNode();
  FocusNode _focusStadium = new FocusNode();
  FocusNode _focusCourtName = new FocusNode();
  int playerNo = 0;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
    _loadPlayerData();
    _loadStatdiumData();
    _temperature();
    checkEnabled();
    _focusPlayerA.addListener(_onFocusChangePlayerA);
    _focusPlayerB.addListener(_onFocusChangePlayerB);
    _focusMatchName.addListener(_onFocusChangeMatchName);
    _focusNumberOfGames.addListener(_onFocusChangeNumberOfGames);
    _focusStadium.addListener(_onFocusChangeStadium);
    _focusCourtName.addListener(_onFocusChangeCourtName);
    focusValue = false;
    _loadPlayerNumber();

    dbTime = "00:00";
    displayedTime = "00 : 00";

    _validateMatchName = true;
    _validatePlayerAName = true;
    _validatePlayerBName = true;
    _validateGameNum = true;
    _validateNoOfSets = true;
    _validateAdvantage = true;
    _validateTieBreack = true;

    textBoderColor = AppColors.form_border;
    errorText = null;
    textBoderColorPlayerA = AppColors.form_border;
    errorTextPlayerA = null;
    textBoderColorPlayerB = AppColors.form_border;
    errorTextPlayerB = null;
    textBoderColorGameNum = AppColors.form_border;
    errorTextGameNum = null;
    textBoderColorNoOfSets = AppColors.form_border;
    errorTextNoOfSets = null;
    textBoderColorAdvantage = AppColors.form_border;
    errorTextAdvantage = null;
    textBoderColorTieBreack = AppColors.form_border;
    errorTextTieBreack = null;

    enableButton = false;
  }

  resetItemsTemp() {
    // print("testing");
    widget.setTempPlayerScores({});
  }

  //load player number
  _loadPlayerNumber() {
    var selectQuery = [
      "SELECT Player_no from player where user_name = ? ORDER BY Player_no DESC",
      [widget.userName],
      {"calledMethod": "selectPlayerNumber"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  void _onFocusChangeCourtName() {
    if (_focusCourtName.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangeStadium() {
    if (_focusStadium.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangePlayerA() {
    if (_focusPlayerA.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangePlayerB() {
    if (_focusPlayerB.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangeMatchName() {
    if (_focusMatchName.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangeNumberOfGames() {
    if (_focusNumberOfGames.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  // LOAD PLAYER DATA --------------------------------------------
  _loadPlayerData() {
    var param1 = [
      "SELECT player_id,Player_no,name FROM player where status = ? and user_name = ?",
      [0, widget.userName],
      {"calledMethod": "_loadPlayerData"}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
  }

  // LOAD STADIUM DATA --------------------------------------------
  _loadStatdiumData() {
    var param2 = [
      "SELECT stadium_id,stadium_name FROM stadium where status = ? and user_name = ?",
      [0, widget.userName],
      {"calledMethod": "_loadStatdiumData"}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param2);
  }

  // LOAD COURT DATA --------------------------------------------
  _loadCourtData() {
    var param3 = [
      "SELECT court_id,court_name,stadium_id,stadium_name FROM court where stadium_id = ? and status = 0",
      [stadium],
      {"calledMethod": "_loadCourtData"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param3);
  }

  //Dropdownvalue
  String drpDownA;
  String drpDownB;
  String drpDownAafterChange;
  String drpDownBafterChange;

  // //Display number
  String displayPlayerANo = "";
  String playerANoAfterChange = "";
  String displayPlayerBNo = "";
  String playerBNoAfterChange = "";

  // INITIALIZING STATE VALUES --------------------------------------------
  static var dateObject = DateTime.now();
  String displayedDate = dateFormatter(dateObject);
  static var timeObject = DateTime.now();
  String displayedTime = "00 : 00 ";
  static String dbTime = "00:00";

  // ----------------------------------
  static const form_width = 280.0;
  static const row_height = 80.0;
  static const box_height = 48.0;
  static const default_font_size = AppStyles.normal_font;
  static const default_padding = 10.0;
  static const text_box_height = (box_height - default_font_size) / 2.0;

  // DISABLED BTN--------------------------
  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;
  String finalMatchID = '';
  var weatherValue = "";
  var tempValue = "";

//Feild error colors
  static var textBoderColor = AppColors.form_border;
  static var errorText;
  static var textBoderColorPlayerA = AppColors.form_border;
  static var errorTextPlayerA;
  static var textBoderColorPlayerB = AppColors.form_border;
  static var errorTextPlayerB;
  static var textBoderColorGameNum = AppColors.form_border;
  static var errorTextGameNum;
  static var textBoderColorNoOfSets = AppColors.form_border;
  static var errorTextNoOfSets;
  static var textBoderColorAdvantage = AppColors.form_border;
  static var errorTextAdvantage;
  static var textBoderColorTieBreack = AppColors.form_border;
  static var errorTextTieBreack;

  //Validation variables
  bool _validateMatchName = true;
  bool _validatePlayerAName = true;
  bool _validatePlayerBName = true;
  bool _validateGameNum = true;
  bool _validateNoOfSets = true;
  bool _validateAdvantage = true;
  bool _validateTieBreack = true;

  static String messageMatchName = "Error in text";
  static String messagePlayerAName = "Error in text";
  static String messagePlayerBName = "Error in text";
  static String messageGameNum = "Error in text";
  static String messageNoOfSets = "Error in text";
  static String messageAdvantage = "Error in text";
  static String messageTieBreack = "Error in text";

  static bool enableButton = false;
  bool courtRead = true;
  var invalColor = AppColors.lightGray;

  static int noOfCourt;
  static int noOfCourtRegisterd;

  static bool courtData = false;

  void checkEnabled() {
    if ((_matchNameController.text.toString() != '') &&
        (_noOfGamesController.text.toString() != '') &&
        (_playerBController.text.toString() != '') &&
        (_playerAController.text.toString() != '') &&
        (noOfSets != null) &&
        (advantage != null) &&
        (tieBreak != null) &&
        (noOfSets != '') &&
        (advantage != '') &&
        (tieBreak != '') &&
        _validateMatchName == true &&
        _validatePlayerAName == true &&
        _validatePlayerBName == true &&
        _validateGameNum == true &&
        _validateNoOfSets == true &&
        _validateAdvantage == true &&
        _validateTieBreack == true) {
      setState(() {
        enableButton = true;
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
      });
    } else {
      setState(() {
        enableButton = false;
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
      });
    }
  }

  // SAVE DATA -------------------------------------------------------
  void insertData() {
    String matchId = Uuid().v1();
    setState(() {
      finalMatchID = matchId;
    });

    if (weather == null) {
      weatherValue = "Not set";
    } else {
      weatherValue = weather;
    }
    if (temp == null) {
      tempValue = "Not set";
    } else {
      tempValue = temp;
    }

    var data = [
      widget.userName,
      matchId,
      _matchNameController.text.toString(),
      playerA,
      playerB,
      _noOfGamesController.text.toString(),
      noOfSets,
      weatherValue,
      tempValue,
      advantage,
      tieBreak,
      stadium,
      court,
      dateObject.toString(),
      dbTime
    ];
    var insertQuery = [
      "INSERT INTO gameSettings (user_name,match_id, match_name, player1, player2, no_of_games, no_of_sets, wheather, temperature, advantage, tie_break, stadium, court_number, game_date, game_time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      data,
      {"calledMethod": 'insertToGameSettings'}
    ];
    if ((_matchNameController.text.toString() != '') &&
        (playerA != '') &&
        (playerB != '') &&
        (playerA != null) &&
        (playerB != null) &&
        (_noOfGamesController.text.toString() != '') &&
        (noOfSets != null) &&
        (advantage != null) &&
        (tieBreak != null) &&
        (noOfSets != '') &&
        (advantage != '') &&
        (tieBreak != '') &&
        _validateMatchName == true &&
        _validatePlayerAName == true &&
        _validatePlayerBName == true &&
        _validateGameNum == true) {
      controller.execFunction(
        ControllerFunc.db_sqlite,
        ControllerSubFunc.db_insert,
        insertQuery,
      );

      //adding dummy data to result table------------------------

      var newData = [matchId, '0', playerA, playerB, '0', '0', 0, 0];
      var param = [
        "INSERT INTO result (match_id,game_time, winner , looser, winner_set, looser_set , game_Status , game_tot_time) VALUES (? , ? , ? , ?, ? , ? , ? , ?)",
        newData
      ];

      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_insert, param);

      if (weather == null) {
        weatherValue = "Not set";
      } else {
        weatherValue = weather;
      }

      if (temp == null) {
        tempValue = "Not set";
      } else {
        tempValue = temp;
      }

      //end of adding data to result table----------------------
      widget.onScoreArgumentSaving({
        'match_id': finalMatchID,
        'player1Id': playerA,
        'player2Id': playerB,
        'noOfGames': _noOfGamesController.text.toString(),
        'noOfSets': noOfSets,
        'tieBreak': tieBreak,
        'advantage': advantage,
        'temperature': tempValue,
        'match_name': _matchNameController.text.toString(),
        'weather': weatherValue,
        'stadium': stadium,
        'court': court,
        'date': dateObject,
        'time': timeObject
      });
      widget.funcSetGameStart(true);
      widget.toggleStartState(false);
      widget.onSelected(RoutingData.Score, true, false);
    }
  }

  void newDataInject() {
    resetItemsTemp();
    int oneValueAtLeastActive = 0;
    List queries = [];
    List params = [];

    if (_playerAController.text != playerAControllerValue) {
      // Player A Update ------------------------------------
      playerA = Uuid().v1();
      queries.add(
          "INSERT INTO player (user_name,player_id,Player_no,name, gender, date_of_birth, handedness, playstyle, team, role, status) VALUES ( ? , ? , ? , ? , ? , ?, ? , ? , ? ,? ,? )");
      params.add([
        widget.userName,
        playerA,
        playerNo,
        _playerAController.text,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        '0'
      ]);
      oneValueAtLeastActive = 1;
      setState(() {
        playerNo++;
      });
    }
    if (_playerBController.text != playerBControllerValue) {
      playerB = Uuid().v1();
      queries.add(
          "INSERT INTO player (user_name,player_id,Player_no,name, gender, date_of_birth, handedness, playstyle, team, role, status) VALUES ( ? , ? , ? , ? , ? , ?, ? , ? , ? ,?,?)");
      params.add([
        widget.userName,
        playerB,
        playerNo,
        _playerBController.text,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        '0'
      ]);
      oneValueAtLeastActive = 1;
    }

    if (_stadiumController.text != stadiumValue) {
      stadium = Uuid().v1();
      queries.add(
          "INSERT INTO stadium (user_name,stadium_id, stadium_name, stadium_address, contact_number, no_of_courts, reservation_url, Stadium_Lat, Stadium_Lon, status) VALUES ( ?, ? , ? , ? , ? , ?, ? , ?,  ?, ? )");
      params.add([
        widget.userName,
        stadium,
        _stadiumController.text,
        '',
        '-',
        0,
        '',
        0,
        0,
        0
      ]);
      oneValueAtLeastActive = 1;
    }

    if (stadium != null &&
        _courtController.text != courtValue &&
        _courtController.text != '' &&
        _courtController.text != null) {
      court = Uuid().v1();
      queries.add(
          "INSERT INTO court (court_id,stadium_id,court_name, serface, indoor, available_time, stadium_name,status) VALUES ( ? , ? , ? , ? , ?, ? , ? , ? )");
      params.add([
        court,
        stadium,
        _courtController.text,
        CommonValues.notSet,
        CommonValues.notSet,
        '0-0',
        _stadiumController.text,
        '0'
      ]);
      oneValueAtLeastActive = 1;
      courtData = true;
    }

    if (oneValueAtLeastActive == 1) {
      var querywithParams = [
        queries,
        params,
        {"calledMethod": 'newDataInject'}
      ];
      controller.execFunction(ControllerFunc.db_sqlite,
          ControllerSubFunc.db_select_batch, querywithParams);
    } else {
      insertData();
    }
  }

  // GET DATA -------------------------------------------------------
  void getData(x) {
    var selectQuery = [
      "SELECT * FROM gameSettings where match_name=?",
      [x]
    ];
    controller.execFunction(
      ControllerFunc.db_sqlite,
      ControllerSubFunc.db_select,
      selectQuery,
    );
  }

  // VERTICAL ITEMPADDING --------------------------------------------
  commonPadding() {
    return EdgeInsets.fromLTRB(0.0, default_padding, 0.0, 0.0);
  }

  // INPUT BOX UPPER TEXT --------------------------------------------
  customFont(text) {
    return Flexible(
      child: Text(
        text,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontSize: default_font_size,
            color: AppColors.black,
            fontWeight: FontWeight.w600),
      ),
    );
  }

  static dateFormatter(date) {
    var selectedyear = date.year;
    var selectedmonth = 'Jan';
    var selectedDate = date.day;
    switch (date.month) {
      case 1:
        selectedmonth = 'Jan';
        break;
      case 2:
        selectedmonth = 'Feb';
        break;
      case 3:
        selectedmonth = 'Mar';
        break;
      case 4:
        selectedmonth = 'Apr';
        break;
      case 5:
        selectedmonth = 'May';
        break;
      case 6:
        selectedmonth = 'Jun';
        break;
      case 7:
        selectedmonth = 'Jul';
        break;
      case 8:
        selectedmonth = 'Aug';
        break;
      case 9:
        selectedmonth = 'Sept';
        break;
      case 10:
        selectedmonth = 'Oct';
        break;
      case 11:
        selectedmonth = 'Nov';
        break;
      case 12:
        selectedmonth = 'Dec';
        break;
      default:
    }
    return '$selectedmonth $selectedDate , $selectedyear';
  }

  // Time FORMAT (eg: 00:00:00) --------------------------------------------

  static timeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    return '$hr : $min';
  }

  static dbtimeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    return '$hr:$min';
  }

  // VERTICAL ITEM - DATE_ADDER --------------------------------------------
  customDate(text, saveIndex) {
    return Align(
      child: Container(
        width: form_width,
        height: row_height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(child: customFont(text)),
            Container(
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(
                  color: AppColors.form_border,
                  width: 1.2,
                  style: BorderStyle.solid,
                ),
              ),
              child: FlatButton(
                onPressed: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2016, 1, 1),
                      maxTime: DateTime(2025, 12, 31),
                      onChanged: (date) {}, onConfirm: (date) {
                    dateFormatter(date);
                    setState(() {
                      dateObject = date;
                      displayedDate = dateFormatter(date);
                    });
                  }, currentTime: dateObject, locale: LocaleType.en);
                },
                child: Center(
                  child: Text(
                    displayedDate,
                    style: TextStyle(
                        fontSize: default_font_size,
                        color: AppColors.black,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // VERTICAL ITEM - TIME_ADDER --------------------------------------------
  DateTime time;
  customTime(text, saveIndex) {
    if (displayedTime == "00 : 00") {
      time = DateTime.now();
    } else {
      time = DateTime.parse(timeObject.toString());
    }

    return Align(
      child: Container(
        width: form_width,
        height: row_height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(child: customFont(text)),
            Container(
                width: double.infinity,
                height: 48,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                    color: AppColors.form_border,
                    width: 1.2,
                    style: BorderStyle.solid,
                  ),
                ),
                child: FlatButton(
                  onPressed: () {
                    showCupertinoModalPopup<void>(
                      context: context,
                      builder: (BuildContext context) {
                        return _buildBottomPicker(
                          CupertinoDatePicker(
                            use24hFormat: true,
                            mode: CupertinoDatePickerMode.time,
                            initialDateTime: time,
                            onDateTimeChanged: (date) {
                              setState(() {
                                timeObject = date;
                                dbTime = dbtimeFormatter(date);
                                displayedTime = timeFormatter(date);
                              });
                            },
                          ),
                        );
                      },
                    );
                  },
                  child: Center(
                    child: Text(
                      displayedTime,
                      style: TextStyle(
                          fontSize: default_font_size,
                          color: AppColors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPicker(Widget picker) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 44,
                child: CupertinoButton(
                  pressedOpacity: 0.3,
                  padding: EdgeInsets.only(right: 16, top: 0),
                  child: Text(
                    'Done',
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    setState(() {
                      timeObject = time;
                      dbTime = dbtimeFormatter(timeObject);
                      displayedTime = timeFormatter(timeObject);
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 200,
          padding: const EdgeInsets.only(top: 0.0),
          color: CupertinoColors.white,
          child: DefaultTextStyle(
            style: const TextStyle(
              color: CupertinoColors.black,
              fontSize: 22.0,
            ),
            child: SafeArea(
              top: true,
              child: picker,
            ),
          ),
        ),
      ],
    );
  }

  commonTxtStyle() {
    return TextStyle(
      fontSize: default_font_size,
      fontFamily: 'Rajdhani',
      color: AppColors.black,
      fontWeight: FontWeight.w500,
    );
  }

  String noOfSets;
  String weather;
  String temp;
  String advantage;
  String tieBreak;
  String stadium;
  String court;
  String playerA;
  String playerB;
  final _matchNameController = new TextEditingController();
  final _noOfGamesController = new TextEditingController();
  final _playerAController = new TextEditingController();
  final _playerBController = new TextEditingController();
  final _stadiumController = new TextEditingController();
  final _courtController = new TextEditingController();
  var playerAControllerValue = '';
  var playerBControllerValue = '';
  var stadiumValue = '';
  var courtValue = '';
  List<KeyValueModel> playerNameList = [];
  List<KeyValueModel> stadiumNameList = [];
  List<KeyValueModel> courtNameList = [];
  bool focusValue = true;

  static var arrNoOFSets = <String>['1', '3', '5'];
  static var arrWeather = <String>[
    CommonValues.notSet,
    CommonValues.clear,
    CommonValues.cloudy,
    CommonValues.rainy,
    CommonValues.snow,
  ];
  static var tempr = <String>[];

  _temperature() {
    List<String> list = new List<String>();
    list.add("Not Set");
    for (var i = 7; i < 45; i++) {
      String count = (i + 1).toString();
      list.add(count + " C");
    }
    setState(() {
      tempr = list;
    });
  }

  static var arrAdvantage = <String>[
    ScoreValues.noAdvantage,
    ScoreValues.advantage,
    ScoreValues.semiAdvantage,
  ];
  static var arrTieBreak = <String>[
    ScoreValues.noTieBreak,
    ScoreValues.tieBreak,
    ScoreValues.superTieBreak
  ];

  List<DropdownMenuItem<KeyValueModel>> loadPlayerList(player) {
    List<DropdownMenuItem<KeyValueModel>> dynamicList = [];

    switch (player) {
      case "A":
        for (var i = 0; i < playerNameList.length; i++) {
          if (playerNameList[i].key == drpDownB) {
            continue;
          }

          dynamicList.add(DropdownMenuItem<KeyValueModel>(
            child: Text(playerNameList[i].key,
                        overflow: TextOverflow.fade, maxLines: 1,
  softWrap: false,),
            value: playerNameList[i],
          ));
        }
        break;

      case "B":
        for (var i = 0; i < playerNameList.length; i++) {
          if (playerNameList[i].key == drpDownA) {
            continue;
          }

          dynamicList.add(DropdownMenuItem<KeyValueModel>(
            child: Text(playerNameList[i].key,
                        overflow: TextOverflow.fade, maxLines: 1,
  softWrap: false,),
            value: playerNameList[i],
          ));
        }
        break;
      default:
        break;
    }

    return dynamicList;
  }

  // MAIN PROGRAMME --------------------------------------------
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: MediaQuery(
          data: MediaQueryData(),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());

              setState(() {
                focusValue = false;
              });
            },
            onPanDown: (details) {
              FocusScope.of(context).requestFocus(FocusNode());

              setState(() {
                focusValue = false;
              });
            },
            child: Container(
                color: Colors.white,
                child: ListView(
                  children: <Widget>[
                    // MATCH NAME AND ROUND ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: TxtBox(
                          topic: 'Match name and round',
                          borderColor: textBoderColor,
                          errorText: errorText,
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  width: box_height,
                                  height: box_height,
                                  child: Center(
                                    child: FlatButton(
                                      onPressed: () {
                                        _matchNameController.clear();

                                        var errorStatus =
                                            TextBoxValidation.isEmpty(
                                                _matchNameController.text);

                                        setState(() {
                                          _validateMatchName =
                                              errorStatus['state'];
                                          messageMatchName =
                                              errorStatus['errorMessage'];
                                        });

                                        if (errorStatus['state'] == false) {
                                          setState(() {
                                            textBoderColor = Colors.red;
                                            errorText =
                                                errorStatus['errorMessage'];
                                          });
                                        } else if (errorStatus['state'] ==
                                            true) {
                                          setState(() {
                                            textBoderColor =
                                                AppColors.form_border;
                                            errorText = null;
                                          });
                                        }

                                        checkEnabled();
                                      },
                                      child: Icon(Icons.close,
                                          size: 20, color: AppColors.black),
                                    ),
                                  ),
                                ),
                              ),
                              Theme(
                                data: Theme.of(context).copyWith(),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      10.0, 0.0, (box_height - 5.0), 0.0),
                                  child: Center(
                                    child: TextField(
                                      maxLength: 25,
                                      focusNode: _focusMatchName,
                                      controller: _matchNameController,
                                      autofocus: false,
                                      onChanged: (String newValue) {
                                        var errorStatus =
                                            TextBoxValidation.isEmpty(
                                                _matchNameController.text);

                                        setState(() {
                                          _validateMatchName =
                                              errorStatus['state'];
                                          messageMatchName =
                                              errorStatus['errorMessage'];
                                        });

                                        if (errorStatus['state'] == false) {
                                          setState(() {
                                            textBoderColor = Colors.red;
                                            errorText =
                                                errorStatus['errorMessage'];
                                          });
                                        } else if (errorStatus['state'] ==
                                            true) {
                                          setState(() {
                                            textBoderColor =
                                                AppColors.form_border;
                                            errorText = null;
                                          });
                                        }

                                        checkEnabled();
                                      },
                                      style: TextStyle(
                                          fontSize: default_font_size,
                                          color: AppColors.black,
                                          fontWeight: FontWeight.w500),
                                      decoration: InputDecoration(
                                        counterText: "",
                                        hintText: 'Enter match name...',
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical:
                                                    ((text_box_height / 2))),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    // PLAYER A ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: TxtBox(
                          topic: 'Player A',
                          borderColor: textBoderColorPlayerA,
                          errorText: errorTextPlayerA,
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 1),
                                child: Text(
                                  displayPlayerANo,
                                  style: commonTxtStyle(),
                                ),
                              ),
                              Flexible(
                                child: Stack(
                                  children: <Widget>[
                                    Positioned(
                                      right: 0,
                                      top: 0,
                                      child: Container(
                                        width: box_height,
                                        height: box_height,
                                        child: Center(
                                          child: Icon(Icons.expand_more,
                                              size: 24, color: Colors.black),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: 0,
                                      top: 0,
                                      width: form_width,
                                      height: box_height,
                                      child: Theme(
                                        data: Theme.of(context).copyWith(),
                                        child: DropdownButtonHideUnderline(
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                default_padding,
                                                0.0,
                                                default_padding,
                                                0.0),
                                            child:
                                                DropdownButton<KeyValueModel>(
                                              isExpanded: true,
                                              iconSize: 0,
                                              style: commonTxtStyle(),
                                              onChanged:
                                                  (KeyValueModel newValue) {
                                                setNameandNo(newValue, "A");

                                                var errorStatus =
                                                    TextBoxValidation.isEmpty(
                                                        _playerAController
                                                            .text);

                                                setState(() {
                                                  _validatePlayerAName =
                                                      errorStatus['state'];
                                                  messagePlayerAName =
                                                      errorStatus[
                                                          'errorMessage'];
                                                });

                                                if (errorStatus['state'] ==
                                                    false) {
                                                  setState(() {
                                                    textBoderColorPlayerA =
                                                        Colors.red;
                                                    errorTextPlayerA =
                                                        errorStatus[
                                                            'errorMessage'];
                                                  });
                                                } else if (errorStatus[
                                                        'state'] ==
                                                    true) {
                                                  setState(() {
                                                    textBoderColorPlayerA =
                                                        AppColors.form_border;
                                                    errorTextPlayerA = null;
                                                  });
                                                }

                                                checkEnabled();
                                              },
                                              items: loadPlayerList("A"),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      right: 0,
                                      child: Center(
                                        child: focusValue
                                            ? Container(
                                                width: box_height,
                                                height: box_height,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                            FocusNode());
                                                    setState(() {
                                                      focusValue = false;
                                                    });
                                                  },
                                                  child: Container(
                                                      color:
                                                          Colors.transparent),
                                                ),
                                              )
                                            : null,
                                      ),
                                    ),
                                    Theme(
                                      data: Theme.of(context).copyWith(),
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            2.0, 0.0, (box_height - 5.0), 0.0),
                                        child: Center(
                                          child: TextField(
                                            maxLength: 25,
                                            focusNode: _focusPlayerA,
                                            autofocus: false,
                                            onChanged: (String newValue) {
                                              nameCleared(newValue, "A");

                                              var errorStatus =
                                                  TextBoxValidation.isEmpty(
                                                      _playerAController.text);

                                              if (errorStatus['state'] ==
                                                  false) {
                                                setState(() {
                                                  _validatePlayerAName =
                                                      errorStatus['state'];
                                                  messagePlayerAName =
                                                      errorStatus[
                                                          'errorMessage'];

                                                  textBoderColorPlayerA =
                                                      Colors.red;
                                                  errorTextPlayerA =
                                                      errorStatus[
                                                          'errorMessage'];
                                                });
                                              }
                                              if (errorStatus['state'] ==
                                                  true) {
                                                _validatePlayerAName =
                                                    errorStatus['state'];
                                                messagePlayerAName =
                                                    errorStatus['errorMessage'];

                                                textBoderColorPlayerA =
                                                    AppColors.form_border;
                                                errorTextPlayerA = null;
                                              }

                                              checkEnabled();
                                            },
                                            controller: _playerAController,
                                            style: TextStyle(
                                                fontSize: default_font_size,
                                                color: AppColors.black,
                                                fontWeight: FontWeight.w500),
                                            decoration: InputDecoration(
                                              counterText: "",
                                              hintText: 'Enter player A...',
                                              contentPadding:
                                                  const EdgeInsets.symmetric(
                                                      vertical:
                                                          ((text_box_height /
                                                              2))),
                                              border: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    // PLAYER B ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: TxtBox(
                          topic: 'Player B',
                          borderColor: textBoderColorPlayerB,
                          errorText: errorTextPlayerB,
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 1),
                                child: Text(
                                  displayPlayerBNo,
                                  style: commonTxtStyle(),
                                ),
                              ),
                              Flexible(
                                child: Stack(
                                  children: <Widget>[
                                    Positioned(
                                      right: 0,
                                      top: 0,
                                      child: Container(
                                        width: box_height,
                                        height: box_height,
                                        child: Center(
                                          child: Icon(Icons.expand_more,
                                              size: 24, color: Colors.black),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: 0,
                                      top: 0,
                                      width: form_width,
                                      height: box_height,
                                      child: Theme(
                                        data: Theme.of(context).copyWith(),
                                        child: DropdownButtonHideUnderline(
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                default_padding,
                                                0.0,
                                                default_padding,
                                                0.0),
                                            child:
                                                DropdownButton<KeyValueModel>(
                                              isExpanded: true,
                                              iconSize: 0,
                                              style: commonTxtStyle(),
                                              onChanged:
                                                  (KeyValueModel newValue) {
                                                setNameandNo(newValue, "B");

                                                var errorStatus =
                                                    TextBoxValidation.isEmpty(
                                                        _playerBController
                                                            .text);

                                                setState(() {
                                                  _validatePlayerBName =
                                                      errorStatus['state'];
                                                  messagePlayerBName =
                                                      errorStatus[
                                                          'errorMessage'];
                                                });

                                                if (errorStatus['state'] ==
                                                    false) {
                                                  setState(() {
                                                    textBoderColorPlayerB =
                                                        Colors.red;
                                                    errorTextPlayerB =
                                                        errorStatus[
                                                            'errorMessage'];
                                                  });
                                                } else if (errorStatus[
                                                        'state'] ==
                                                    true) {
                                                  setState(() {
                                                    textBoderColorPlayerB =
                                                        AppColors.form_border;
                                                    errorTextPlayerB = null;
                                                  });
                                                }

                                                checkEnabled();
                                              },
                                              items: loadPlayerList("B"),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      right: 0,
                                      child: Center(
                                        child: focusValue
                                            ? Container(
                                                width: box_height,
                                                height: box_height,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                            FocusNode());
                                                    setState(() {
                                                      focusValue = false;
                                                    });
                                                  },
                                                  child: Container(
                                                      color:
                                                          Colors.transparent),
                                                ),
                                              )
                                            : null,
                                      ),
                                    ),
                                    Theme(
                                      data: Theme.of(context).copyWith(),
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            2.0, 0.0, (box_height - 5.0), 0.0),
                                        child: Center(
                                          child: TextField(
                                            maxLength: 25,
                                            focusNode: _focusPlayerB,
                                            autofocus: false,
                                            onChanged: (String newValue) {
                                              nameCleared(newValue, "B");

                                              var errorStatus =
                                                  TextBoxValidation.isEmpty(
                                                      _playerBController.text);

                                              if (errorStatus['state'] ==
                                                  false) {
                                                setState(() {
                                                  _validatePlayerBName =
                                                      errorStatus['state'];
                                                  messagePlayerBName =
                                                      errorStatus[
                                                          'errorMessage'];

                                                  textBoderColorPlayerB =
                                                      Colors.red;
                                                  errorTextPlayerB =
                                                      errorStatus[
                                                          'errorMessage'];
                                                });
                                              }
                                              if (errorStatus['state'] ==
                                                  true) {
                                                setState(() {
                                                  _validatePlayerBName =
                                                      errorStatus['state'];
                                                  messagePlayerBName =
                                                      errorStatus[
                                                          'errorMessage'];

                                                  textBoderColorPlayerB =
                                                      AppColors.form_border;
                                                  errorTextPlayerB = null;
                                                });
                                              }

                                              checkEnabled();
                                            },
                                            controller: _playerBController,
                                            style: TextStyle(
                                                fontSize: default_font_size,
                                                color: AppColors.black,
                                                fontWeight: FontWeight.w500),
                                            decoration: InputDecoration(
                                              counterText: "",
                                              hintText: 'Enter player B...',
                                              contentPadding:
                                                  const EdgeInsets.symmetric(
                                                      vertical:
                                                          ((text_box_height /
                                                              2))),
                                              border: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

// NO OF GAMES ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: TxtBox(
                          topic: 'Number of games',
                          borderColor: textBoderColorGameNum,
                          errorText: errorTextGameNum,
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  width: box_height,
                                  height: box_height,
                                  child: Center(
                                    child: FlatButton(
                                      onPressed: () {
                                        _noOfGamesController.clear();

                                        var errorStatus =
                                            TextBoxValidation.isEmpty(
                                                _noOfGamesController.text);

                                        setState(() {
                                          _validateGameNum =
                                              errorStatus['state'];
                                          messageGameNum =
                                              errorStatus['errorMessage'];
                                        });

                                        if (errorStatus['state'] == false) {
                                          setState(() {
                                            textBoderColorGameNum = Colors.red;
                                            errorTextGameNum =
                                                errorStatus['errorMessage'];
                                          });
                                        } else if (errorStatus['state'] ==
                                            true) {
                                          setState(() {
                                            textBoderColorGameNum =
                                                AppColors.form_border;
                                            errorTextGameNum = null;
                                          });
                                        }

                                        checkEnabled();
                                      },
                                      child: Icon(Icons.close,
                                          size: 20, color: AppColors.black),
                                    ),
                                  ),
                                ),
                              ),
                              Theme(
                                data: Theme.of(context).copyWith(),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      10.0, 0.0, (box_height - 5.0), 0.0),
                                  child: Center(
                                    child: TextField(
                                      focusNode: _focusNumberOfGames,
                                      keyboardType: TextInputType.number,
                                      autofocus: false,
                                      onChanged: (String newValue) {
                                        var errorStatus = [
                                          TextBoxValidation.isEmpty(
                                              _noOfGamesController.text),
                                          TextBoxValidation.isDivisibleByTwo(
                                              _noOfGamesController.text)
                                        ];

                                        if (errorStatus[0]['state'] == true) {
                                          if (errorStatus[1]['state'] == true) {
                                            setState(() {
                                              _validateGameNum =
                                                  errorStatus[1]['state'];
                                              textBoderColorGameNum =
                                                  AppColors.form_border;
                                              errorTextGameNum = null;
                                            });
                                          } else {
                                            setState(() {
                                              _validateGameNum =
                                                  errorStatus[1]['state'];
                                              messageGameNum = errorStatus[1]
                                                  ['errorMessage'];

                                              textBoderColorGameNum =
                                                  Colors.red;
                                              errorTextGameNum = errorStatus[1]
                                                  ['errorMessage'];
                                            });
                                          }
                                        } else {
                                          setState(() {
                                            _validateGameNum =
                                                errorStatus[0]['state'];
                                            messageGameNum =
                                                errorStatus[0]['errorMessage'];

                                            textBoderColorGameNum = Colors.red;
                                            errorTextGameNum =
                                                errorStatus[0]['errorMessage'];
                                          });
                                        }

                                        checkEnabled();
                                      },
                                      controller: _noOfGamesController,
                                      style: TextStyle(
                                          fontSize: default_font_size,
                                          color: AppColors.black,
                                          fontWeight: FontWeight.w500),
                                      decoration: InputDecoration(
                                        hintText:
                                            'Enter the number of games...',
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical:
                                                    ((text_box_height / 2))),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

// NO OF SETS ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            child: DrpDown(
                              topic: 'No of sets',
                              errorText: errorTextNoOfSets,
                              borderColor: textBoderColorNoOfSets,
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: noOfSets,
                                iconSize: 0,
                                hint: Text('Set no of sets...'),
                                style: commonTxtStyle(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    noOfSets = newValue;
                                  });

                                  if (noOfSets == null) {
                                    setState(() {
                                      _validateNoOfSets = false;
                                      messageNoOfSets =
                                          "This field is Required!";
                                      textBoderColorNoOfSets = Colors.red;
                                      errorTextNoOfSets = messageNoOfSets;
                                    });
                                  } else {
                                    setState(() {
                                      _validateNoOfSets = true;
                                      messageNoOfSets = null;
                                      textBoderColorNoOfSets =
                                          AppColors.form_border;
                                      errorTextNoOfSets = null;
                                    });
                                  }
                                  checkEnabled();
                                },
                                items:
                                    arrNoOFSets.map<DropdownMenuItem<String>>(
                                  (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  },
                                ).toList(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                            child: focusValue
                                ? Container(
                                    height: box_height + 30,
                                    child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          focusValue = false;
                                        });
                                      },
                                      child:
                                          Container(color: Colors.transparent),
                                    ),
                                  )
                                : null,
                          ),
                        ],
                      ),
                    ),
                    // WHEATHER ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            child: DrpDown(
                              topic: 'Weather',
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: weather,
                                iconSize: 0,
                                hint: Text('Set the whather type...'),
                                style: commonTxtStyle(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    weather = newValue;
                                  });
                                  checkEnabled();
                                },
                                items: arrWeather.map<DropdownMenuItem<String>>(
                                  (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  },
                                ).toList(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                            child: focusValue
                                ? Container(
                                    height: box_height + 30,
                                    child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          focusValue = false;
                                        });
                                      },
                                      child:
                                          Container(color: Colors.transparent),
                                    ),
                                  )
                                : null,
                          ),
                        ],
                      ),
                    ),
                    // Temperature ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            child: DrpDown(
                              topic: 'Temperature in Celsius',
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: temp,
                                iconSize: 0,
                                hint: Text('Enter the temperature...'),
                                style: commonTxtStyle(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    temp = newValue;
                                  });
                                  checkEnabled();
                                },
                                items: tempr.map<DropdownMenuItem<String>>(
                                  (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  },
                                ).toList(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                            child: focusValue
                                ? Container(
                                    height: box_height + 30,
                                    child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          focusValue = false;
                                        });
                                      },
                                      child:
                                          Container(color: Colors.transparent),
                                    ),
                                  )
                                : null,
                          ),
                        ],
                      ),
                    ),
                    // ADVANTAGE ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            child: DrpDown(
                              topic: 'Advantage',
                              errorText: errorTextAdvantage,
                              borderColor: textBoderColorAdvantage,
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: advantage,
                                iconSize: 0,
                                hint: Text('Set advantage...'),
                                style: commonTxtStyle(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    advantage = newValue;
                                  });

                                  if (advantage == null) {
                                    setState(() {
                                      _validateAdvantage = false;
                                      messageAdvantage =
                                          "This field is Required!";
                                      textBoderColorAdvantage = Colors.red;
                                      errorTextAdvantage = messageAdvantage;
                                    });
                                  } else {
                                    setState(() {
                                      _validateAdvantage = true;
                                      messageAdvantage = null;
                                      textBoderColorAdvantage =
                                          AppColors.form_border;
                                      errorTextAdvantage = null;
                                    });
                                  }

                                  checkEnabled();
                                },
                                items:
                                    arrAdvantage.map<DropdownMenuItem<String>>(
                                  (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  },
                                ).toList(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                            child: focusValue
                                ? Container(
                                    height: box_height + 30,
                                    child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          focusValue = false;
                                        });
                                      },
                                      child:
                                          Container(color: Colors.transparent),
                                    ),
                                  )
                                : null,
                          ),
                        ],
                      ),
                    ),
                    // TIE BREAK ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            child: DrpDown(
                              topic: 'Tie break',
                              errorText: errorTextTieBreack,
                              borderColor: textBoderColorTieBreack,
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: tieBreak,
                                iconSize: 0,
                                hint: Text('Set tie break...'),
                                style: commonTxtStyle(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    tieBreak = newValue;
                                  });

                                  if (tieBreak == null) {
                                    setState(() {
                                      _validateTieBreack = false;
                                      messageTieBreack =
                                          "This field is Required!";
                                      textBoderColorTieBreack = Colors.red;
                                      errorTextTieBreack = messageAdvantage;
                                    });
                                  } else {
                                    setState(() {
                                      _validateTieBreack = true;
                                      messageTieBreack = null;
                                      textBoderColorTieBreack =
                                          AppColors.form_border;
                                      errorTextTieBreack = null;
                                    });
                                  }
                                  checkEnabled();
                                },
                                items:
                                    arrTieBreak.map<DropdownMenuItem<String>>(
                                  (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  },
                                ).toList(),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                            child: focusValue
                                ? Container(
                                    height: box_height + 30,
                                    child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          focusValue = false;
                                        });
                                      },
                                      child:
                                          Container(color: Colors.transparent),
                                    ),
                                  )
                                : null,
                          ),
                        ],
                      ),
                    ),

                    // STADIUM ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: TxtBox(
                          topic: 'Stadium',
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  width: box_height,
                                  height: box_height,
                                  child: Center(
                                    child: Icon(Icons.expand_more,
                                        size: 24, color: Colors.black),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: 0,
                                width: form_width,
                                height: box_height,
                                child: Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: DropdownButtonHideUnderline(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          default_padding,
                                          0.0,
                                          default_padding,
                                          0.0),
                                      child: DropdownButton<KeyValueModel>(
                                        isExpanded: true,
                                        iconSize: 0,
                                        style: commonTxtStyle(),
                                        onChanged: (KeyValueModel newValue) {
                                          setState(() {
                                            _stadiumController.text =
                                                newValue.key;
                                            stadiumValue = newValue.key;
                                            stadium = newValue.value;
                                          });

                                          _courtController.clear();
                                          _loadCourtData();

                                          if (_stadiumController.text == "" ||
                                              _stadiumController.text == null) {
                                            courtRead = true;
                                            invalColor = AppColors.lightGray;
                                          } else {
                                            courtRead = false;
                                            invalColor = AppColors.white;
                                          }

                                          checkEnabled();
                                        },
                                        items: stadiumNameList
                                            .map((KeyValueModel item) {
                                          return DropdownMenuItem<
                                              KeyValueModel>(
                                            child: Text(item.key,
                                                    overflow: TextOverflow.fade, 
                                                    maxLines: 1,
                                                    softWrap: false,),
                                            value: item,
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 0,
                                child: Center(
                                  child: focusValue
                                      ? Container(
                                          width: box_height,
                                          height: box_height,
                                          child: GestureDetector(
                                            onTap: () {
                                              FocusScope.of(context)
                                                  .requestFocus(FocusNode());
                                              setState(() {
                                                focusValue = false;
                                              });
                                            },
                                            child: Container(
                                                color: Colors.transparent),
                                          ),
                                        )
                                      : null,
                                ),
                              ),
                              Theme(
                                data: Theme.of(context).copyWith(),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      10.0, 2.5, (box_height - 5.0), 0.0),
                                  child: TextField(
                                    maxLength: 25,
                                    focusNode: _focusStadium,
                                    autofocus: false,
                                    onChanged: (String newValue) {
                                      var hasItem = stadiumNameList.firstWhere(
                                          (KeyValueModel item) =>
                                              item.key == newValue,
                                          orElse: () => null);

                                      if (hasItem != null) {
                                        setState(() {
                                          stadiumValue = hasItem.key;
                                          stadium = hasItem.value;
                                        });
                                        _courtController.clear();
                                        _loadCourtData();
                                      } else {
                                        _courtController.clear();
                                        clearCourtData();
                                      }

                                      if (_stadiumController.text == "" ||
                                          _stadiumController.text == null) {
                                        courtRead = true;
                                        invalColor = AppColors.lightGray;
                                      } else {
                                        courtRead = false;
                                        invalColor = AppColors.white;
                                      }

                                      checkEnabled();
                                    },
                                    controller: _stadiumController,
                                    style: TextStyle(
                                        fontSize: default_font_size,
                                        color: AppColors.black,
                                        fontWeight: FontWeight.w500),
                                    decoration: InputDecoration(
                                      counterText: "",
                                      hintText: 'Enter stadium...',
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: ((text_box_height/2))),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    // COURT ------------------------------------------------------------
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: TxtBox(
                          topic: 'Court Name / NO',
                          invalidColor: invalColor,
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  width: box_height,
                                  height: box_height,
                                  child: Center(
                                    child: Icon(Icons.expand_more,
                                        size: 24, color: Colors.black),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: 0,
                                width: form_width,
                                height: box_height,
                                child: Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: DropdownButtonHideUnderline(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          default_padding,
                                          0.0,
                                          default_padding,
                                          0.0),
                                      child: DropdownButton<KeyValueModel>(
                                        isExpanded: true,
                                        iconSize: 0,
                                        style: commonTxtStyle(),
                                        onChanged: (KeyValueModel newValue) {
                                          setState(() {
                                            _courtController.text =
                                                newValue.key;
                                            courtValue = newValue.key;
                                            court = newValue.value;
                                          });
                                          _loadCourtData();
                                          checkEnabled();
                                        },
                                        items: courtNameList
                                            .map((KeyValueModel item) {
                                          return DropdownMenuItem<
                                              KeyValueModel>(
                                            child: Text(item.key,
                                                    overflow: TextOverflow.fade, 
                                                    maxLines: 1,
                                                    softWrap: false,),
                                            value: item,
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 0,
                                child: Center(
                                  child: focusValue
                                      ? Container(
                                          width: box_height,
                                          height: box_height,
                                          child: GestureDetector(
                                            onTap: () {
                                              FocusScope.of(context)
                                                  .requestFocus(FocusNode());
                                              setState(() {
                                                focusValue = false;
                                              });
                                            },
                                            child: Container(
                                                color: Colors.transparent),
                                          ),
                                        )
                                      : null,
                                ),
                              ),
                              Theme(
                                data: Theme.of(context).copyWith(),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      10.0, 2.5, (box_height - 5.0), 0.0),
                                  child: TextField(
                                    maxLength: 25,
                                    readOnly: courtRead,
                                    focusNode: _focusCourtName,
                                    autofocus: false,
                                    onChanged: (String newValue) {
                                      checkEnabled();
                                    },
                                    controller: _courtController,
                                    style: TextStyle(
                                        fontSize: default_font_size,
                                        color: AppColors.black,
                                        fontWeight: FontWeight.w500),
                                    decoration: InputDecoration(
                                      counterText: "",
                                      hintText: 'Set court...',
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: ((text_box_height/2))),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: customDate("Game date", 10),
                      ),
                    ),
                    Padding(
                      padding: commonPadding(),
                      child: Container(
                        child: customTime("Starting time", 11),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          0.0, 2 * default_padding, 0.0, 0.0),
                      child: Align(
                        child: Container(
                          width: form_width,
                          height: box_height,
                          child: RaisedButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0),
                                side: BorderSide(
                                    color: disabledBtnFont,
                                    width: 1,
                                    style: BorderStyle.solid)),
                            onPressed: () {
                              if (enableButton == true) {
                                newDataInject();
                              } else {
                                _validationCheck();
                              }
                            },
                            color: disabledBtn,
                            child: Text(
                              'New Game',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: disabledBtnFont,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          0.0, default_padding, 0.0, (6 * default_padding)),
                    ),
                  ],
                )),
          )),
      onWillPop: () async => Future.value(false),
    );
  }

//Check the vakidation in button click------------------
  _validationCheck() {
    //match name----------
    var errorStatusMatchName =
        TextBoxValidation.isEmpty(_matchNameController.text);

    if (errorStatusMatchName['state'] == false) {
      setState(() {
        _validateMatchName = errorStatusMatchName['state'];
        messageMatchName = errorStatusMatchName['errorMessage'];
        textBoderColor = Colors.red;
        errorText = errorStatusMatchName['errorMessage'];
      });
    } else {}

//Player A----------------
    var errorStatusPlayerA = TextBoxValidation.isEmpty(_playerAController.text);

    if (errorStatusPlayerA['state'] == false) {
      setState(() {
        _validatePlayerAName = errorStatusPlayerA['state'];
        messagePlayerAName = errorStatusPlayerA['errorMessage'];
        textBoderColorPlayerA = Colors.red;
        errorTextPlayerA = errorStatusPlayerA['errorMessage'];
      });
    } else {}

//Player B------------
    var errorStatusPalyerB = TextBoxValidation.isEmpty(_playerBController.text);

    if (errorStatusPalyerB['state'] == false) {
      setState(() {
        _validatePlayerBName = errorStatusPalyerB['state'];
        messagePlayerBName = errorStatusPalyerB['errorMessage'];
        textBoderColorPlayerB = Colors.red;
        errorTextPlayerB = errorStatusPalyerB['errorMessage'];
      });
    } else {}

//Num of Games--------------
    var errorStatusNoGame =
        TextBoxValidation.isEmpty(_noOfGamesController.text);

    if (errorStatusNoGame['state'] == false) {
      setState(() {
        _validateGameNum = errorStatusNoGame['state'];
        messageGameNum = errorStatusNoGame['errorMessage'];
        textBoderColorGameNum = Colors.red;
        errorTextGameNum = errorStatusNoGame['errorMessage'];
      });
    } else {}

//Num of set--------------

    if (noOfSets == null) {
      setState(() {
        _validateNoOfSets = false;
        messageNoOfSets = "This field is Required!";
        textBoderColorNoOfSets = Colors.red;
        errorTextNoOfSets = messageNoOfSets;
      });
    } else {}

//Advantage------------
    if (advantage == null) {
      setState(() {
        _validateAdvantage = false;
        messageAdvantage = "This field is Required!";
        textBoderColorAdvantage = Colors.red;
        errorTextAdvantage = messageAdvantage;
      });
    } else {}

//Tie Breack------------
    if (tieBreak == null) {
      setState(() {
        _validateTieBreack = false;
        messageTieBreack = "This field is Required!";
        textBoderColorTieBreack = Colors.red;
        errorTextTieBreack = messageAdvantage;
      });
    } else {}
  }

  setPlayerList(data) async {
    data.forEach((i) {
      setState(() {
        String pName;
        if (i['Player_no'] < 10) {
          pName = "0" + i['Player_no'].toString() + " - " + i['name'];
        } else {
          pName = i['Player_no'].toString() + " - " + i['name'];
        }

        playerNameList
            .add(KeyValueModel(key: (pName), value: (i['player_id'])));
      });
    });
  }

  setStadiumList(data) async {
    data.forEach((i) {
      setState(() {
        stadiumNameList.add(
            KeyValueModel(key: (i['stadium_name']), value: (i['stadium_id'])));
      });
    });
  }

  setCourtList(data) async {
    await clearCourtData();

    setState(() {
      data.forEach((i) {
        courtNameList
            .add(KeyValueModel(key: (i['court_name']), value: (i['court_id'])));
      });
    });
  }

  clearCourtData() {
    setState(() {
      courtNameList.clear();
    });
  }

  setNameandNo(newValue, player) {
    var text = newValue.key;
    List<String> data = text.split(" - ");

    switch (player) {
      case "A":
        setState(() {
          drpDownA = newValue.key;
          drpDownAafterChange = newValue.key;
          playerANoAfterChange = data[0];
          displayPlayerANo = data[0] + " - ";
          _playerAController.text = data[1];
          playerAControllerValue = data[1];
          playerA = newValue.value;
        });
        break;

      case "B":
        setState(() {
          drpDownB = newValue.key;
          drpDownBafterChange = newValue.key;
          playerBNoAfterChange = data[0];
          displayPlayerBNo = data[0] + " - ";
          _playerBController.text = data[1];
          playerBControllerValue = data[1];
          playerB = newValue.value;
        });
        break;
    }
  }

  nameCleared(newValue, player) {
    switch (player) {
      case "A":
        setState(() {
          if (newValue == "") {
            drpDownA = "";
            drpDownAafterChange = "";
            playerANoAfterChange = "";
            displayPlayerANo = "";
            _playerAController.text = "";
            playerAControllerValue = "";
            playerA = "";
          } else {
            if (newValue != playerAControllerValue) {
              drpDownA = "";
              displayPlayerANo = "";
            } else {
              if (
                  // playerBControllerValue != "" &&
                  newValue != playerBControllerValue) {
                drpDownA = drpDownAafterChange;
                displayPlayerANo = playerANoAfterChange + " - ";
              } else {
                playerAControllerValue = "";
              }
            }
          }
        });
        break;
      case "B":
        setState(() {
          if (newValue == "") {
            drpDownB = "";
            drpDownBafterChange = "";
            playerBNoAfterChange = "";
            displayPlayerBNo = "";
            _playerBController.text = "";
            playerBControllerValue = "";
            playerB = "";
          } else {
            if (newValue != playerBControllerValue) {
              drpDownB = "";
              displayPlayerBNo = "";
            } else {
              if (
                  // playerAControllerValue != "" &&
                  newValue != playerAControllerValue) {
                drpDownB = drpDownBafterChange;
                displayPlayerBNo = playerBNoAfterChange + " - ";
              } else {
                playerBControllerValue = "";
              }
            }
          }
        });
        break;
      default:
        break;
    }
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            switch (response['calledMethod']) {
              case '_loadPlayerData':
                setPlayerList(response['response_data']);
                break;
              case '_loadStatdiumData':
                setStadiumList(response['response_data']);
                break;
              case '_loadCourtData':
                setCourtList(response['response_data']);

                //number of courts in stadium table
                var param2 = [
                  "SELECT no_of_courts FROM stadium  where stadium_id = ?",
                  [stadium],
                  {"calledMethod": 'selectNoFoCourts'}
                ];

                controller.execFunction(ControllerFunc.db_sqlite,
                    ControllerSubFunc.db_select, param2);

                //number of courts registerd
                var param3 = [
                  "SELECT COUNT(*) FROM court  where stadium_id = ? AND status = ?",
                  [stadium, 0],
                  {"calledMethod": 'selectNoFoCourtRegisterd'}
                ];

                controller.execFunction(ControllerFunc.db_sqlite,
                    ControllerSubFunc.db_select, param3);

                break;

              case 'newDataInject':
                insertData();
                break;
              case 'selectPlayerNumber':
                if (response['response_data'].toString() != "null") {
                  if (response['response_data'].toString() != "[]") {
                    int x = response['response_data'][0]['Player_no'] + 1;
                    if (x < 10) {
                      setState(() {
                        playerNo = x;
                      });
                    } else {
                      setState(() {
                        playerNo = x;
                      });
                    }
                  } else {
                    setState(() {
                      playerNo = 1;
                    });
                  }
                }

                break;

              case 'selectNoFoCourts':
                setState(() {
                  noOfCourt = response['response_data'][0]['no_of_courts'];
                });

                break;

              case 'selectNoFoCourtRegisterd':
                setState(() {
                  noOfCourtRegisterd = response['response_data'][0]['COUNT(*)'];
                });

                break;

              case 'insertToGameSettings':
                if (courtData == true) {
                  if (noOfCourt == null) {
                    int newCourtCount;
                    newCourtCount = 1;

                    var param = [
                      "UPDATE stadium SET no_of_courts = ?  where stadium_id = ?",
                      [newCourtCount, stadium],
                      {"calledMethod": 'updateStadium'}
                    ];

                    controller.execFunction(ControllerFunc.db_sqlite,
                        ControllerSubFunc.db_update, param);
                  } else {
                    if (noOfCourt == noOfCourtRegisterd) {
                      int newCourtCount;
                      newCourtCount = noOfCourt + 1;

                      var param = [
                        "UPDATE stadium SET no_of_courts = ?  where stadium_id = ?",
                        [newCourtCount, stadium],
                        {"calledMethod": 'updateStadium'}
                      ];

                      controller.execFunction(ControllerFunc.db_sqlite,
                          ControllerSubFunc.db_update, param);
                    } else {}
                  }
                  courtData = false;
                } else {}

                break;

              default:
                break;
            }
          } else {}
        }
        break;
      default:
        {
          // print('NOT SQLITE METHOD');
        }
    }
  }
}

//Create a Model class to hold key-value pair data
class KeyValueModel {
  String key;
  String value;

  KeyValueModel({this.key, this.value});
}
