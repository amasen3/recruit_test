import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:tenizo/const.dart';
import 'package:tenizo/controller/game_setting_data.dart';
import 'package:tenizo/model/color_loader.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tenizo/util/app_const.dart';

import 'package:flutter_slidable/flutter_slidable.dart';

import '../custom/commom_popup.dart';

class Home extends StatefulWidget {
  final Function onSelected;
  final Function onArgumentSaving;
  final Function toggleStartState;
  final Function onScoreArgumentSaving;
  final Function setTempPlayerScores;
  final Function onPageChanged;
  final initAd;
  final showAd;
  final String userName;
  final Function funcSetGameTimeInSeconds;
  final Function funcSetplayerServe;

  @override
  _HomeState createState() => _HomeState();
  Home(
      {Key key,
      this.onSelected,
      this.onArgumentSaving,
      this.toggleStartState,
      this.onScoreArgumentSaving,
      this.setTempPlayerScores,
      this.onPageChanged,
      this.initAd,
      this.showAd,
      this.userName,
      this.funcSetplayerServe,
      this.funcSetGameTimeInSeconds})
      : super(key: key);
}

class _HomeState extends State<Home> with BaseControllerListner {
  BaseController controller;
  SlidableController slidableController;

  static var gamedatastatus = 0;
  static var playerdatastatus = 0;
  static var stadiumdatastatus = 0;
  static var resultDatastatus = 0;
  static var scoreatastatus = 0;
  static var scorelaststatus = 0;
  static var gameSettingsEnd = 0;

  static var gameSettingsEndFinish = 0;
  static var scoreDataEndFinish = 0;
  static var score1DataEndFinish = 0;

  static var value = 0;

  String matchId;

  //bool loader = false;

  List<Widget> _listOfHistory = [];
  List<GameSettingData> _listGame = [];
  List<PlayerData> _listPlayer = [];
  List<ResultData> _listResult = [];
  List<ScoreData> _listScoreData = [];
  List<String> matchIds = [];
  bool _loading = true;
  int count =0;

  @override
  void initState() {
    super.initState();

    if (Const.edOfGame == true) {
      widget.showAd(0);
      Const.edOfGame = false;
    } else {}

    // player1setList = [];
    // player2setList = [];
    // tieBreakOnListPlayer1 = [];
    // tieBreakOnListPlayer2 = [];
    // swap = [];

    controller = new BaseController(this);
    _loadData();

    setState(() {
      gameSettingsEnd = 0;
      count = 0;
    });
  }

  var val;

  _loadData() {
    setState(() {
      val = 1;
    });
    var param2 = [
      "SELECT * FROM gameSettings where user_name = ?",
      [widget.userName],
      {"calledMethod": 'GameSettings'}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param2);
  }

  setGameData(data) async {
    data.forEach((i) {
      var date = i['game_date'].split(' ');
      matchIds.add(i['match_id']);

      _listGame.add(GameSettingData(
        matchID: i['match_id'],
        matchName: i['match_name'],
        player1: i['player1'],
        player2: i['player2'],
        noOfSets: i['no_of_sets'],
        advantage: i['advantage'],
        tieBreak: i['tie_break'],
        gameDate: date[0],
        noOfGames: i['no_of_games'],
        wheather: i['wheather'],
        stadium: i['stadium'],
        courtNumber: i['court_number'],
      ));
    });
  }

  setPlayerData(data) {
    data.forEach((i) {
      _listPlayer.add(PlayerData(
          playerId: i['player_id'],
          name: i['name'],
          playerImage: i['player_image']));
    });
  }

  setResultData(data) {
    data.forEach((i) {
      _listResult.add(ResultData(
          matchId: i['match_id'],
          winner: i['winner'],
          looser: i['looser'],
          gameStatus: i['game_Status'],
          gameTotTime: i['game_tot_time']));
    });
  }

  setScoreSetData(data) {
    data.forEach((i) {
      _listScoreData.add(ScoreData(
        matchId: i['match_id'],
        won: i['won'],
        serve: i['serve'],
        player1Sets: i['player1Sets'],
        player1Games: i['player1Games'],
        player1Points: i['player1Points'],
        player2Sets: i['player2Sets'],
        player2Games: i['player2Games'],
        player2Points: i['player2Points'],
        totalSets: i['totalSets'],
        totalGames: i['totalGames'],
        setFinished: i['setFinished'],
        tieBreakOn: i['tieBreakOn'],
      ));
    });
  }

  int scoreServe = 0;
  var scorePlayer1;
  var scorePlayer2;
  int scorePlayer1Sets = 0;
  int scorePlayer1Games = 0;
  int scorePlayer1Points = 0;
  int scorePlayer2Sets = 0;
  int scorePlayer2Games = 0;
  int scorePlayer2Points = 0;
  int scoreTotalSets = 0;
  int scoreTotalGames = 0;
  bool breakProcessToTieBreak = false;

  int player1noOfWonsInSet1 = 0;
  int player1noOfWonsInSet2 = 0;
  int player1noOfWonsInSet3 = 0;
  int player1noOfWonsInSet4 = 0;
  int player1noOfWonsInSet5 = 0;
  int player2noOfWonsInSet1 = 0;
  int player2noOfWonsInSet2 = 0;
  int player2noOfWonsInSet3 = 0;
  int player2noOfWonsInSet4 = 0;
  int player2noOfWonsInSet5 = 0;

  var i1sAngle1Status = false;
  var i1sAngle2Status = false;
  var i1sAngle3Status = false;
  var i1sAngle4Status = false;
  var i1sAngle5Status = false;
  var i2sAngle1Status = false;
  var i2sAngle2Status = false;
  var i2sAngle3Status = false;
  var i2sAngle4Status = false;
  var i2sAngle5Status = false;

  var weather;
  var stadium;
  var court;
  var temperature;

  var trigerTieAngles = false;

  int player1TieBreak1 = 0;
  int player1TieBreak2 = 0;
  int player1TieBreak3 = 0;
  int player1TieBreak4 = 0;
  int player1TieBreak5 = 0;
  int player2TieBreak1 = 0;
  int player2TieBreak2 = 0;
  int player2TieBreak3 = 0;
  int player2TieBreak4 = 0;
  int player2TieBreak5 = 0;

  bool semiAdvantageSecond;
  bool player1inAd;
  bool player2inAd;

  double innerRadius;

  var x1tie1 = false;
  var x1tie2 = false;
  var x1tie3 = false;
  var x1tie4 = false;
  var x1tie5 = false;
  var x2tie1 = false;
  var x2tie2 = false;
  var x2tie3 = false;
  var x2tie4 = false;
  var x2tie5 = false;

  var tieBreak;

  static int gameTotalTime;

  setPauseData(data, String type) {
    if (type == "GameSettingsEnd") {
      gameSettingsEndFinish = 1;

      widget.onScoreArgumentSaving({
        'match_id': data[0]['match_id'],
        'player1Id': data[0]['player1'],
        'player2Id': data[0]['player2'],
        'noOfGames': data[0]['no_of_games'],
        'noOfSets': data[0]['no_of_sets'],
        'tieBreak': data[0]['tie_break'],
        'advantage': data[0]['advantage'],
        'match_name': data[0]['match_name'],
        'weather': data[0]['wheather'],
        'temperature': data[0]['temperature'],
        'stadium': data[0]['stadium'],
        'court': data[0]['court_number'],
        'date': dateObject,
        'time': timeObject
      });

      weather = data[0]['wheather'];
      temperature = data[0]['temperature'];
      stadium = data[0]['stadium'];
      court = data[0]['court_number'];
      tieBreak = data[0]['tie_break'];
      count++;
    }

    else if (type == "scoreDataEnd") {
      scoreDataEndFinish = 1;

      if (data.length != 0) {
        scoreServe = data[0]['serve'];

        if (data[0]['gameFinished'] == 0) {
          if (scoreServe == 1) {
            scorePlayer1 = true;
            scorePlayer2 = false;
            widget.funcSetplayerServe(1);
          } else {
            scorePlayer2 = true;
            scorePlayer1 = false;
            widget.funcSetplayerServe(2);
          }
        } else {
          if (scoreServe == 1) {
            scorePlayer1 = false;
            scorePlayer2 = true;
            widget.funcSetplayerServe(2);
          } else {
            scorePlayer2 = false;
            scorePlayer1 = true;
            widget.funcSetplayerServe(1);
          }
        }

        scorePlayer1Sets = data[0]['player1Sets'];
        scorePlayer1Games = (data[0]['setFinished'] != 1)?data[0]['player1Games']:0;
        scorePlayer1Points = (data[0]['setFinished'] != 1)?data[0]['player1Points']:0;
        scorePlayer2Sets = data[0]['player2Sets'];
        scorePlayer2Games = (data[0]['setFinished'] != 1)?data[0]['player2Games']:0;
        scorePlayer2Points = (data[0]['setFinished'] != 1)?data[0]['player2Points']:0;
        scoreTotalSets = data[0]['totalSets'];
        scoreTotalGames = data[0]['totalGames'];
        breakProcessToTieBreak = (data[0]['breakProcessToTieBreak'] == 1)?true:false;
        player1inAd = (data[0]['player1Points'] == 41)?true:false;
        player2inAd = (data[0]['player2Points'] == 41)?true:false;
        //semi advantage add
        semiAdvantageSecond = data[0]['semiAdvantageSecond']==1?true:false;
        //print( data[0]['semiAdvantageSecond']);

      } else {
        scorePlayer1 = true;
        scorePlayer2 = false;
        widget.funcSetplayerServe(1);

        scorePlayer1Sets = 0;
        scorePlayer1Games = 0;
        scorePlayer1Points = 0;
        scorePlayer2Sets = 0;
        scorePlayer2Games = 0;
        scorePlayer2Points = 0;
        scoreTotalSets = 0;
        scoreTotalGames = 0;
        semiAdvantageSecond = false;

      }
      count++;
    }
    else if (type == "score1DataEnd") {
      score1DataEndFinish = 1;

      List<ScoreData> _scores;
      var dataplayer1set = [];
      var dataplayer2set = [];

      var dataplayer1Point = [];
      var dataplayer2Point = [];
      var tieBreakOn = [];

      if (data.length != 0) {
        _scores = _listScoreData.where((value) {
          return value.matchId == data[0]['match_id'];
        }).toList();

        dataplayer1set = _scores.map((data) => data.player1Games).toList();
        dataplayer2set = _scores.map((data) => data.player2Games).toList();

        dataplayer1Point = _scores.map((data) => data.player1Points).toList();
        dataplayer2Point = _scores.map((data) => data.player2Points).toList();
        tieBreakOn = _scores.map((data) => data.tieBreakOn).toList();
      } else {}

      //Set data visible--------------------------------------------

      if (dataplayer1set.length == 0) {
        player2noOfWonsInSet1 = 0;
        player2noOfWonsInSet2 = 0;
        player2noOfWonsInSet3 = 0;
        player2noOfWonsInSet4 = 0;
        player2noOfWonsInSet5 = 0;

        i2sAngle1Status = false;
        i2sAngle2Status = false;
        i2sAngle3Status = false;
        i2sAngle4Status = false;
        i2sAngle5Status = false;
      } else if (dataplayer1set.length == 1) {
        player1noOfWonsInSet1 = dataplayer1set[0];

        i1sAngle1Status = true;
      } else if (dataplayer1set.length == 2) {
        player1noOfWonsInSet1 = dataplayer1set[0];
        player1noOfWonsInSet2 = dataplayer1set[1];

        i1sAngle1Status = true;
        i1sAngle2Status = true;
      } else if (dataplayer1set.length == 3) {
        player1noOfWonsInSet1 = dataplayer1set[0];
        player1noOfWonsInSet2 = dataplayer1set[1];
        player1noOfWonsInSet3 = dataplayer1set[2];

        i1sAngle1Status = true;
        i1sAngle2Status = true;
        i1sAngle3Status = true;
      } else if (dataplayer1set.length == 4) {
        player1noOfWonsInSet1 = dataplayer1set[0];
        player1noOfWonsInSet2 = dataplayer1set[1];
        player1noOfWonsInSet3 = dataplayer1set[2];
        player1noOfWonsInSet4 = dataplayer1set[3];

        i1sAngle1Status = true;
        i1sAngle2Status = true;
        i1sAngle3Status = true;
        i1sAngle4Status = true;
      } else if (dataplayer1set.length == 5) {
        player1noOfWonsInSet1 = dataplayer1set[0];
        player1noOfWonsInSet2 = dataplayer1set[1];
        player1noOfWonsInSet3 = dataplayer1set[2];
        player1noOfWonsInSet4 = dataplayer1set[3];
        player1noOfWonsInSet5 = dataplayer1set[4];

        i1sAngle1Status = true;
        i1sAngle2Status = true;
        i1sAngle3Status = true;
        i1sAngle4Status = true;
        i1sAngle5Status = true;
      } else {}

      if (dataplayer2set.length == 1) {
        player2noOfWonsInSet1 = dataplayer2set[0];

        i2sAngle1Status = true;
      } else if (dataplayer2set.length == 2) {
        player2noOfWonsInSet1 = dataplayer2set[0];
        player2noOfWonsInSet2 = dataplayer2set[1];

        i2sAngle1Status = true;
        i2sAngle2Status = true;
      } else if (dataplayer2set.length == 3) {
        player2noOfWonsInSet1 = dataplayer2set[0];
        player2noOfWonsInSet2 = dataplayer2set[1];
        player2noOfWonsInSet3 = dataplayer2set[2];

        i2sAngle1Status = true;
        i2sAngle2Status = true;
        i2sAngle3Status = true;
      } else if (dataplayer2set.length == 4) {
        player2noOfWonsInSet1 = dataplayer2set[0];
        player2noOfWonsInSet2 = dataplayer2set[1];
        player2noOfWonsInSet3 = dataplayer2set[2];
        player2noOfWonsInSet4 = dataplayer2set[3];

        i2sAngle1Status = true;
        i2sAngle2Status = true;
        i2sAngle3Status = true;
        i2sAngle4Status = true;
      } else if (dataplayer2set.length == 5) {
        player2noOfWonsInSet1 = dataplayer2set[0];
        player2noOfWonsInSet2 = dataplayer2set[1];
        player2noOfWonsInSet3 = dataplayer2set[2];
        player2noOfWonsInSet4 = dataplayer2set[3];
        player2noOfWonsInSet5 = dataplayer2set[4];

        i2sAngle1Status = true;
        i2sAngle2Status = true;
        i2sAngle3Status = true;
        i2sAngle4Status = true;
        i2sAngle5Status = true;
      } else {}

      //end Set data--------------------------------------------

      //tieBreak visible-----------------------------------

      if (tieBreakOn.length == 0) {
        player1TieBreak1 = 0;
        player1TieBreak2 = 0;
        player1TieBreak3 = 0;
        player1TieBreak4 = 0;
        player1TieBreak5 = 0;
        player2TieBreak1 = 0;
        player2TieBreak2 = 0;
        player2TieBreak3 = 0;
        player2TieBreak4 = 0;
        player2TieBreak5 = 0;

        x1tie1 = false;
        x2tie1 = false;
      } else if (tieBreakOn.length == 1) {
        player1TieBreak1 = dataplayer1Point[0];
        player2TieBreak1 = dataplayer2Point[0];

        if (tieBreakOn[0] == 1) {
          x1tie1 = true;
          x2tie1 = true;
        }
      } else if (tieBreakOn.length == 2) {
        player1TieBreak1 = dataplayer1Point[0];
        player1TieBreak2 = dataplayer1Point[1];

        player2TieBreak1 = dataplayer2Point[0];
        player2TieBreak2 = dataplayer2Point[1];

        if (tieBreakOn[0] == 1) {
          x1tie1 = true;
          x2tie1 = true;
        }
        if (tieBreakOn[1] == 1) {
          x1tie2 = true;
          x2tie2 = true;
        }
      } else if (tieBreakOn.length == 3) {
        player1TieBreak1 = dataplayer1Point[0];
        player1TieBreak2 = dataplayer1Point[1];
        player1TieBreak3 = dataplayer1Point[2];

        player2TieBreak1 = dataplayer2Point[0];
        player2TieBreak2 = dataplayer2Point[1];
        player2TieBreak3 = dataplayer2Point[2];

        if (tieBreakOn[0] == 1) {
          x1tie1 = true;
          x2tie1 = true;
        }
        if (tieBreakOn[1] == 1) {
          x1tie2 = true;
          x2tie2 = true;
        }
        if (tieBreakOn[2] == 1) {
          x1tie3 = true;
          x2tie3 = true;
        }
      } else if (tieBreakOn.length == 4) {
        player1TieBreak1 = dataplayer1Point[0];
        player1TieBreak2 = dataplayer1Point[1];
        player1TieBreak3 = dataplayer1Point[2];
        player1TieBreak4 = dataplayer1Point[3];

        player2TieBreak1 = dataplayer2Point[0];
        player2TieBreak2 = dataplayer2Point[1];
        player2TieBreak3 = dataplayer2Point[2];
        player2TieBreak4 = dataplayer2Point[3];

        if (tieBreakOn[0] == 1) {
          x1tie1 = true;
          x2tie1 = true;
        }
        if (tieBreakOn[1] == 1) {
          x1tie2 = true;
          x2tie2 = true;
        }
        if (tieBreakOn[2] == 1) {
          x1tie3 = true;
          x2tie3 = true;
        }
        if (tieBreakOn[3] == 1) {
          x1tie4 = true;
          x2tie4 = true;
        }
      } else if (tieBreakOn.length == 5) {
        player1TieBreak1 = dataplayer1Point[0];
        player1TieBreak2 = dataplayer1Point[1];
        player1TieBreak3 = dataplayer1Point[2];
        player1TieBreak4 = dataplayer1Point[3];
        player1TieBreak5 = dataplayer1Point[4];
        player2TieBreak1 = dataplayer2Point[0];
        player2TieBreak2 = dataplayer2Point[1];
        player2TieBreak3 = dataplayer2Point[2];
        player2TieBreak4 = dataplayer2Point[3];
        player2TieBreak5 = dataplayer2Point[4];

        if (tieBreakOn[0] == 1) {
          x1tie1 = true;
          x2tie1 = true;
        }
        if (tieBreakOn[1] == 1) {
          x1tie2 = true;
          x2tie2 = true;
        }
        if (tieBreakOn[2] == 1) {
          x1tie3 = true;
          x2tie3 = true;
        }
        if (tieBreakOn[3] == 1) {
          x1tie4 = true;
          x2tie4 = true;
        }
        if (tieBreakOn[4] == 1) {
          x1tie5 = true;
          x2tie5 = true;
        }
      } else {}

      //end tiebreak---------------------------

      if (tieBreak == "No Tie Break") {
        innerRadius = 70;
      } else {
        innerRadius = 88;
      }
      count++;
    }

    widget.setTempPlayerScores({
      'player1serve': scorePlayer1,
      'player2serve': scorePlayer2,
      'player1noOfSets': scorePlayer1Sets,
      'player1noOfGames': scorePlayer1Games,
      'player1noOfPoints': scorePlayer1Points,
      'player2noOfSets': scorePlayer2Sets,
      'player2noOfPoints': scorePlayer2Points,
      'player2noOfGames': scorePlayer2Games,
      'totalSets': scoreTotalSets,
      'totalGames': scoreTotalGames,
      'player1inAd': false,
      'player2inAd': false,
      'player1noOfWonsInSet1': player1noOfWonsInSet1,
      'player1noOfWonsInSet2': player1noOfWonsInSet2,
      'player1noOfWonsInSet3': player1noOfWonsInSet3,
      'player1noOfWonsInSet4': player1noOfWonsInSet4,
      'player1noOfWonsInSet5': player1noOfWonsInSet5,
      'player2noOfWonsInSet1': player2noOfWonsInSet1,
      'player2noOfWonsInSet2': player2noOfWonsInSet2,
      'player2noOfWonsInSet3': player2noOfWonsInSet3,
      'player2noOfWonsInSet4': player2noOfWonsInSet4,
      'player2noOfWonsInSet5': player2noOfWonsInSet5,
      'i1sAngle1Status': i1sAngle1Status,
      'i1sAngle2Status': i1sAngle2Status,
      'i1sAngle3Status': i1sAngle3Status,
      'i1sAngle4Status': i1sAngle4Status,
      'i1sAngle5Status': i1sAngle5Status,
      'i2sAngle1Status': i2sAngle1Status,
      'i2sAngle2Status': i2sAngle2Status,
      'i2sAngle3Status': i2sAngle3Status,
      'i2sAngle4Status': i2sAngle4Status,
      'i2sAngle5Status': i2sAngle5Status,
      'breakProcessToTieBreak': breakProcessToTieBreak,
      'weather': weather,
      'temperature': temperature,
      'stadium': stadium,
      'court': court,
      'date': dateObject,
      'time': gameTotalTime,
      'trigerTieAngles': trigerTieAngles,
      'player1TieBreak1': player1TieBreak1,
      'player1TieBreak2': player1TieBreak2,
      'player1TieBreak3': player1TieBreak3,
      'player1TieBreak4': player1TieBreak4,
      'player1TieBreak5': player1TieBreak5,
      'player2TieBreak1': player2TieBreak1,
      'player2TieBreak2': player2TieBreak2,
      'player2TieBreak3': player2TieBreak3,
      'player2TieBreak4': player2TieBreak4,
      'player2TieBreak5': player2TieBreak5,
      'innerRadius': innerRadius,
      'x1tie1': x1tie1,
      'x1tie2': x1tie2,
      'x1tie3': x1tie3,
      'x1tie4': x1tie4,
      'x1tie5': x1tie5,
      'x2tie1': x2tie1,
      'x2tie2': x2tie2,
      'x2tie3': x2tie3,
      'x2tie4': x2tie4,
      'x2tie5': x2tie5,
      'semiAdvantageSecond':semiAdvantageSecond,
    });

    widget.funcSetGameTimeInSeconds(gameTotalTime);

    if (gameSettingsEndFinish != 0 &&
        scoreDataEndFinish != 0 &&
        score1DataEndFinish != 0 &&
        semiAdvantageSecond != null && count == 3) {

      widget.toggleStartState(false);
      // widget.onSelected(RoutingData.Score, true, true);
      widget.onPageChanged(RoutingData.Score, true, true);
    } else {}
  }

  static List<int> player1set;
  //static List<List<int>> player1setList = [];
  //static List<List<int>> player2setList = [];
  static List<int> player2set;
  static List<int> player1PointList;
  static List<int> player2PointList;
  static List<int> tieBreakOnList;
  //static List<List<int>> tieBreakOnListPlayer1 = [];
  //static List<List<int>> tieBreakOnListPlayer2 = [];

  //List<bool> swap = [];

  static var gameState;

  List<String> matchIdArray = [];

  listOfPlayer1Set() {
    //player1setList.add(player1set);
    List<Widget> _list = [];
    if (player1set.length == 0) {
      _list.add(Column(children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8.0),
        ),
        Container(
          padding: EdgeInsets.only(left: 15.0),
        ),
      ]));
    } else {
      if (gameState == "End") {
        _list.add(
          Column(
            children: <Widget>[
              Padding(
                padding: paddingData3,
                child: Container(
                  width: winWidth,
                  height: winHeight,
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: new AssetImage("images/award.png"),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      } else {
        _list.add(
          Column(
            children: <Widget>[
              Padding(
                padding: paddingData3,
                child: Container(
                  width: loosWidth,
                  height: loosHeight,
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                  ),
                ),
              ),
            ],
          ),
        );
      }

      var count = -1;

      var player1PointData = '1';

      player1set.forEach((i) {
        count += 1;
        if (tieBreakOnList[count] == 0) {
          player1PointData = "";
        } else if (tieBreakOnList[count] == 1) {
          player1PointData = "(" + player1PointList[count].toString() + ")";
        } else {}

        _list.add(Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 8.0),
          ),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 3.0),
                    child: Container(
                      width: 20,
                      padding: paddingData4,
                      child: new Text(
                        i.toString(),
                        style: TextStyle(
                            fontSize: 20,
                            color: AppColors.black,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Rajdhani'),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Container(
                    width: 18,
                    padding: EdgeInsets.only(left: 0.0),
                    child: new Text(
                      player1PointData,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 12,
                          color: AppColors.black,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Rajdhani'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ]));
      });
    }
    return _list;
  }

  listOfPlayer2Set() {
    //player2setList.add(player2set);
    // print(tieBreakOnList);
    List<Widget> _list = [];
    if (player2set.length == 0) {
      _list.add(Column(children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8.0),
        ),
        Container(
          padding: EdgeInsets.only(left: 15.0),
        ),
      ]));
    } else {
      if (gameState == "End") {
        _list.add(
          Column(
            children: <Widget>[
              Padding(
                padding: paddingData3,
                child: Container(
                  width: loosWidth,
                  height: loosHeight,
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: new AssetImage("images/red.png"),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      } else {
        _list.add(
          Column(
            children: <Widget>[
              Padding(
                padding: paddingData3,
                child: Container(
                  width: loosWidth,
                  height: loosHeight,
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                  ),
                ),
              ),
            ],
          ),
        );
      }

      var count = -1;
      var player2PointData = '1';

      player2set.forEach((i) {
        count += 1;
        if (tieBreakOnList[count] == 0) {
          player2PointData = "";
        } else if (tieBreakOnList[count] == 1) {
          player2PointData = "(" + player2PointList[count].toString() + ")";
        } else {}

        _list.add(Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 8.0),
          ),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 3.0),
                    child: Container(
                      width: 20,
                      padding: paddingData4,
                      child: new Text(
                        i.toString(),
                        style: TextStyle(
                            fontSize: 20,
                            color: AppColors.black,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Rajdhani'),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Container(
                    width: 18,
                    padding: EdgeInsets.only(left: 0.0),
                    child: new Text(
                      player2PointData,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12,
                          color: AppColors.black,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Rajdhani'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ]));
      });
    }
    return _list;
  }

  deleteAllData(List<String> matchId) {
    List queries = [];
    List params = [];

    queries.add("DELETE FROM scoreData WHERE match_id = ?");
    params.add([matchId[0]]);
    queries.add("DELETE FROM gameSettings WHERE match_id = ?");
    params.add([matchId[0]]);
    queries.add("DELETE FROM result WHERE match_id = ?");
    params.add([matchId[0]]);

    var querywithParams = [
      queries,
      params,
      {"calledMethod": 'deleteAllData'}
    ];

    controller.execFunction(ControllerFunc.db_sqlite,
        ControllerSubFunc.db_select_batch, querywithParams);

    // when pause game deleted redirect to gamesetting from score page
    if (matchId[1] == "0") {
      widget.setTempPlayerScores({});
      widget.toggleStartState(true);
    }
  }

  static var dateObject = DateTime.now();
  static var timeObject = DateTime.now();

  setDataList() {
    _listGame.asMap().forEach((index, s) {
      //Winner & looser names------
      String _winner = _listResult.firstWhere((value) {
        return value.matchId == s.matchID;
      }).winner;
      String _looser = _listResult.firstWhere((value) {
        return value.matchId == s.matchID;
      }).looser;

      String _player1 = _listPlayer.firstWhere((value) {
        return value.playerId == _winner;
      }).name;

      var _player1Image = _listPlayer.firstWhere((value) {
        return value.playerId == _winner;
      }).playerImage;

      var _player2Image = _listPlayer.firstWhere((value) {
        return value.playerId == _looser;
      }).playerImage;

      String _player2 = _listPlayer.firstWhere((value) {
        return value.playerId == _looser;
      }).name;

      int _endorNot = _listResult.firstWhere((value) {
        return value.matchId == s.matchID;
      }).gameStatus;

      String _stateGame;
      if (_endorNot == 1) {
        _stateGame = "End";
      } else {
        _stateGame = "Pause";
      }

      setState(() {
        gameState = _stateGame;
      });

      //game time--------------

      int _gameTotTime = _listResult.firstWhere((value) {
        return value.matchId == s.matchID;
      }).gameTotTime;

//Score data ---------------

      List<ScoreData> _scores = _listScoreData.where((value) {
        return value.matchId == s.matchID;
      }).toList();

//Set values-------------

      if (_winner == s.player1) {
        setState(() {
          player1set = _scores.map((data) => data.player1Games).toList();
          player2set = _scores.map((data) => data.player2Games).toList();

          player1PointList = _scores.map((data) => data.player1Points).toList();
          player2PointList = _scores.map((data) => data.player2Points).toList();
          tieBreakOnList = _scores.map((data) => data.tieBreakOn).toList();
          tieBreakOnList.add(10);
          // tieBreakOnListPlayer1.add(player1PointList);
          // tieBreakOnListPlayer2.add(player2PointList);
          // swap.add(false);
        });
      }

      if (_looser == s.player1) {
        setState(() {
          player1set = _scores.map((data) => data.player2Games).toList();
          player2set = _scores.map((data) => data.player1Games).toList();

          player1PointList = _scores.map((data) => data.player2Points).toList();
          player2PointList = _scores.map((data) => data.player1Points).toList();
          tieBreakOnList = _scores.map((data) => data.tieBreakOn).toList();
          tieBreakOnList.add(10);
          // tieBreakOnListPlayer1.add(player1PointList);
          // tieBreakOnListPlayer2.add(player2PointList);
          // swap.add(true);
        });
      }

      ImageProvider getImageProvider(File f) {
        return f.existsSync()
            ? FileImage(f)
            : const AssetImage("images/user.png");
      }

      setState(() {
        //match id list

        matchIdArray.add(s.matchID);

        _listOfHistory.add(
          Stack(
            alignment: Alignment.topRight,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(6.0, 7.0, 5.0, 5.0),
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Column(
                  children: <Widget>[
//First row---------------------
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                      child: Container(
                        width: currentWidth,
                        child: Row(children: <Widget>[
//Date--------------------

                          Column(children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left: 0.0, right: 0.0),
                            ),
                            Container(
                              width: 100,
                              padding: EdgeInsets.only(left: 0.0, right: 10.0),
                              child: new Text(
                                s.gameDate,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Rajdhani'),
                              ),
                            ),
                          ]),
//Match name--------------------
                          Column(children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(right: 10.0),
                            ),
                            Container(
                              width: currentWidth - 140,
                              padding: EdgeInsets.only(right: 10.0),
                              child: new Text(
                                s.matchName,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Rajdhani'),
                              ),
                            ),
                          ]),
                        ]),
                      ),
                    ),

//First Player -------------------------
                    Row(children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            width: currentWidth - 20,
                            child: Row(
                              children: <Widget>[
//Picture-----------------------
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(12, 8, 0, 0),
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                            width: profWidth,
                                            height: profHeight,
                                            padding: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              color: AppColors.white,
                                              border: Border.all(
                                                color: AppColors.primary_color,
                                                width: 2,
                                              ),
                                              shape: BoxShape.circle,
                                              image: new DecorationImage(
                                                fit: BoxFit.fill,
                                                image: _player1Image != null
                                                    ? getImageProvider(
                                                        File(_player1Image))
                                                    : new AssetImage(
                                                        "images/user.png"),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),

//other data------------------------------------

                                Row(
                                  children: listOfPlayer1Set(),
                                ),
                              ],
                            ),
                          ),

//palyer name------------------------
                          Row(
                            children: <Widget>[
                              Container(
                                width: currentWidth - 20,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Column(
                                      children: <Widget>[
                                        Container(
                                          width: currentWidth - 140,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0),
                                            child: new Text(
                                              _player1,
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.primary_color,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Rajdhani'),
                                                overflow: TextOverflow.ellipsis,

                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  child: new Text(
                                                    gameState,
                                                    style: TextStyle(
                                                        fontSize: 20,
                                                        color: AppColors
                                                            .primary_color,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontFamily: 'Rajdhani'),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: Container(
                                                    width: 35.0,
                                                    height: 35.0,
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: AppColors
                                                          .ternary_color,
                                                      image:
                                                          new DecorationImage(
                                                        fit: BoxFit.fill,
                                                        image: new AssetImage(
                                                            "images/fast-forward.png"),
                                                      ),
                                                    ),
                                                    child: new FlatButton(
                                                      onPressed: () {
                                                        if (_endorNot == 0) {
                                                          setState(() {
                                                            matchId = s.matchID;
                                                          });
                                                          var param2 = [
                                                            "SELECT * FROM gameSettings where match_id = ?",
                                                            [s.matchID],
                                                            {
                                                              "calledMethod":
                                                                  'GameSettingsEnd'
                                                            }
                                                          ];

                                                          controller.execFunction(
                                                              ControllerFunc
                                                                  .db_sqlite,
                                                              ControllerSubFunc
                                                                  .db_select,
                                                              param2);

                                                          var param3 = [
                                                            "SELECT * FROM scoreData where match_id=? ORDER BY id DESC LIMIT 1",
                                                            [s.matchID],
                                                            {
                                                              "calledMethod":
                                                                  'scoreDataEnd'
                                                            }
                                                          ];

                                                          controller.execFunction(
                                                              ControllerFunc
                                                                  .db_sqlite,
                                                              ControllerSubFunc
                                                                  .db_select,
                                                              param3);

                                                          var param4 = [
                                                            "SELECT * FROM scoreData where setFinished = ? and match_id= ? ",
                                                            [1, s.matchID],
                                                            {
                                                              "calledMethod":
                                                                  'score1DataEnd'
                                                            }
                                                          ];

                                                          controller.execFunction(
                                                              ControllerFunc
                                                                  .db_sqlite,
                                                              ControllerSubFunc
                                                                  .db_select,
                                                              param4);

                                                          setState(() {
                                                            gameTotalTime =
                                                                _gameTotTime;
                                                          });
                                                        } else {
                                                          //add display count-----
                                                          if (Const
                                                                  .buttonCount <
                                                              9) {
                                                            Const.buttonCount =
                                                                Const.buttonCount +
                                                                    1;
                                                          } else {
                                                            Const.buttonCount =
                                                                0;
                                                          }

                                                          widget
                                                              .onArgumentSaving({
                                                            "matchID":
                                                                s.matchID,
                                                            "player1":
                                                                s.player1,
                                                            "player2":
                                                                s.player2,
                                                            "time": s.gameTime,
                                                          });

                                                          widget.onSelected(
                                                              RoutingData.Stats,
                                                              false,
                                                              true);
                                                        }
                                                      },
                                                      child: null,
                                                      shape: RoundedRectangleBorder(
                                                          side: BorderSide(
                                                              color: AppColors
                                                                  .ternary_color,
                                                              width: 1,
                                                              style: BorderStyle
                                                                  .solid),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      50)),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ]),

//Second Player----------------------------------
                    Row(children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            width: currentWidth - 20,
                            child: Row(
                              children: <Widget>[
//Picture-----------------------
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(12, 8, 0, 0),
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                            width: profWidth,
                                            height: profHeight,
                                            padding: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              color: AppColors.white,
                                              border: Border.all(
                                                color: AppColors.primary_color,
                                                width: 2,
                                              ),
                                              shape: BoxShape.circle,
                                              image: new DecorationImage(
                                                fit: BoxFit.fill,
                                                image: _player2Image != null
                                                    ? getImageProvider(
                                                        File(_player2Image))
                                                    : new AssetImage(
                                                        "images/user.png"),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),

//other data------------------------------------
                                Row(
                                  children: listOfPlayer2Set(),
                                ),
                              ],
                            ),
                          ),

//palyer name------------------------
                          Row(
                            children: <Widget>[
                              Container(
                                width: currentWidth - 20,
                                child: Row(
                                  children: <Widget>[
                                    Column(
                                      children: <Widget>[
                                        Container(
                                          width: currentWidth - 140,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 15.0, top: 10),
                                            child: new Text(
                                              _player2,
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.primary_color,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'Rajdhani'),
                                                  overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ]),
                  ],
                ),
              )
            ],
          ),
        );
      });
    });
  }

  static var deviceWidth = 0.0;
  static var currentWidth = 0.0;
  static var deviceHeight = 0.0;
  static var currentHeight = 0.0;

  var paddingData;
  var paddingData1;
  var paddingData2;
  var paddingData3;
  var paddingData4;

  var profWidth = 50.0;
  var profHeight = 50.0;

  var winWidth = 20.0;
  var winHeight = 20.0;

  var loosWidth = 20.0;
  var loosHeight = 20.0;

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      setState(() {
        currentWidth = 300;
      });
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 5.0);
      paddingData4 = EdgeInsets.only(left: 10.0);

      profWidth = 35.0;
      profHeight = 35.0;

      winWidth = 15.0;
      winHeight = 15.0;

      loosWidth = 15.0;
      loosHeight = 15.0;
    } else if (deviceWidth <= 400) {
      currentWidth = 340;

      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 5.0);
      paddingData4 = EdgeInsets.only(top: 3.0);

      profWidth = 35.0;
      profHeight = 35.0;

      winWidth = 15.0;
      winHeight = 15.0;

      loosWidth = 15.0;
      loosHeight = 15.0;
    } else if (deviceWidth <= 450) {
      currentWidth = 380;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 5.0);
      paddingData4 = EdgeInsets.only(top: 3.0);

      profWidth = 40.0;
      profHeight = 40.0;

      winWidth = 15.0;
      winHeight = 15.0;

      loosWidth = 15.0;
      loosHeight = 15.0;
    } else if (deviceWidth <= 500) {
      currentWidth = 450;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 15.0);
      paddingData4 = EdgeInsets.only(top: 3.0);
    } else if (deviceWidth <= 550) {
      currentWidth = 450;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 15.0);
      paddingData4 = EdgeInsets.only(top: 3.0);
    } else if (deviceWidth <= 650) {
      currentWidth = 550;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 15.0);
      paddingData4 = EdgeInsets.only(top: 3.0);
    } else if (deviceWidth <= 750) {
      currentWidth = 650;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 15.0);
      paddingData4 = EdgeInsets.only(top: 3.0);
    } else if (deviceWidth <= 800) {
      currentWidth = 700;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 15.0);
      paddingData4 = EdgeInsets.only(top: 3.0);
    } else if (deviceWidth > 800) {
      // for tab
      currentWidth = 830;
      paddingData3 = const EdgeInsets.only(top: 15.0, bottom: 15.0, left: 15.0);
      paddingData4 = EdgeInsets.only(top: 3.0);
    } else {
      currentWidth = 600;
    }

    if (deviceHeight <= 500) {
      currentHeight = 400;
      paddingData = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 0),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 6),
          right: ((currentWidth / 12) * 3));
    } else if ((deviceHeight <= 600)) {
      currentHeight = 400;
      paddingData = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 0),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 6),
          right: ((currentWidth / 12) * 3));
    } else if ((deviceHeight <= 700)) {
      currentHeight = 450;
      paddingData = EdgeInsets.only(
          top: 50.0,
          left: ((currentWidth / 12) * 0),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 6),
          right: ((currentWidth / 12) * 3));
    } else if ((deviceHeight <= 800)) {
      currentHeight = 450;
      paddingData = EdgeInsets.only(
          top: 50.0,
          left: ((currentWidth / 12) * 0),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 6),
          right: ((currentWidth / 12) * 3));
    } else if ((deviceHeight <= 900)) {
      currentHeight = 500;
      paddingData = EdgeInsets.only(
          top: 60.0,
          left: ((currentWidth / 12) * 0),
          right: ((currentWidth / 12) * 10));
      // paddingData =  EdgeInsets.only(top: 60.0, left: ((currentWidth/12)*4), right:  ((currentWidth/12)*7));
      // paddingData =  EdgeInsets.only(top: 60.0, left: ((currentWidth/12)*6), right:  ((currentWidth/12)*3));
      paddingData1 = EdgeInsets.only(
          top: 80.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 80.0,
          left: ((currentWidth / 12) * 6),
          right: ((currentWidth / 12) * 3));
    } else if ((deviceHeight <= 1000)) {
      currentHeight = 550;
      paddingData = EdgeInsets.only(
          top: 60.0,
          left: ((currentWidth / 12) * 0),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 80.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 80.0,
          left: ((currentWidth / 12) * 6),
          right: ((currentWidth / 12) * 3));
    } else if (deviceHeight > 1000) {
      // for tab
      currentHeight = 650;
      paddingData = EdgeInsets.only(
          top: 110.0,
          left: ((currentWidth / 12) * 0.5),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 130.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
      paddingData2 = EdgeInsets.only(
          top: 130.0,
          left: ((currentWidth / 12) * 6.5),
          right: ((currentWidth / 12) * 3));
    } else {
      currentHeight = 500;
      paddingData = EdgeInsets.only(
          top: 100.0,
          left: ((currentWidth / 12) * 0.5),
          right: ((currentWidth / 12) * 10));
      paddingData1 = EdgeInsets.only(
          top: 70.0,
          left: ((currentWidth / 12) * 4),
          right: ((currentWidth / 12) * 7));
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: _loading
            ? Container(
                color: Colors.white, child: Center(child: ColorLoader()))
            : LayoutBuilder(
                builder: (ctx, c) {
                  if (gamedatastatus == 0 || resultDatastatus == 0) {
                    if (playerdatastatus == 0) {
                      return Scaffold(
                        backgroundColor: Color(0xFFE5F9E0),
                        body: SizedBox.expand(
                          child: Container(
                            alignment: Alignment.center,
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10.0, right: 6),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.0, right: 5, bottom: 80),
                                      child: Container(
                                        width: currentWidth,
                                        height: currentHeight - 230,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          border: Border.all(
                                            color: Colors.white,
                                            style: BorderStyle.solid,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Add Your First Player",
                                            style: TextStyle(
                                                fontSize: 30,
                                                color: AppStyle.color_Head,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: 'Rajdhani'),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: paddingData,
                                    child: Container(
                                      width: 50,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image: new AssetImage(
                                              "images/down_arrow.png"),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    } else if (stadiumdatastatus == 0) {
                      return Scaffold(
                        backgroundColor: Color(0xFFE5F9E0),
                        body: SizedBox.expand(
                          child: Container(
                            alignment: Alignment.center,
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10.0, right: 6),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.0, right: 5, bottom: 60),
                                      child: Container(
                                        width: currentWidth,
                                        height: currentHeight - 230,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          border: Border.all(
                                            color: Colors.white,
                                            style: BorderStyle.solid,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Add Your First Stadium",
                                            style: TextStyle(
                                                fontSize: 30,
                                                color: AppStyle.color_Head,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: 'Rajdhani'),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: paddingData1,
                                    child: Container(
                                      width: 50,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image: new AssetImage(
                                              "images/down_arrow.png"),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    } else {
                      return Scaffold(
                        backgroundColor: Color(0xFFE5F9E0),
                        body: SizedBox.expand(
                          child: Container(
                            alignment: Alignment.center,
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10.0, right: 6),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.0, right: 5, bottom: 60),
                                      child: Container(
                                        width: currentWidth,
                                        height: currentHeight - 230,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          border: Border.all(
                                            color: Colors.white,
                                            style: BorderStyle.solid,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Add Your First Match",
                                            style: TextStyle(
                                                fontSize: 30,
                                                color: AppStyle.color_Head,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: 'Rajdhani'),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: paddingData2,
                                    child: Container(
                                      width: 50,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image: new AssetImage(
                                              "images/down_arrow.png"),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                  } else {
                    return Scaffold(
                      backgroundColor: AppColors.secondary_color,
                      body: Column(
                        children: <Widget>[
                          Expanded(
                            child: Center(
                              child: OrientationBuilder(
                                builder: (context, orientation) => _buildList(
                                    context,
                                    orientation == Orientation.portrait
                                        ? Axis.vertical
                                        : Axis.horizontal),

                              //           ListView(
                              // padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                              // children:
                              //     _listOfHistory.map((card) => card).toList(),
                                        
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) async {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (subFunc == ControllerSubFunc.db_select) {
            if (response["calledMethod"] == "GameSettings") {
              setGameData(response['response_data']);
              setState(() {
                gamedatastatus = response['response_data'].length;
              });

              var param1 = [
                "SELECT * FROM player",
                [],
                {"calledMethod": 'Players'}
              ];

              controller.execFunction(ControllerFunc.db_sqlite,
                  ControllerSubFunc.db_select, param1);
            }
            //all player data-----------------
            else if (response["calledMethod"] == "Players") {
              await setPlayerData(response['response_data']);

              var param3 = [
                "SELECT * FROM result",
                [],
                {"calledMethod": 'result'}
              ];

              controller.execFunction(ControllerFunc.db_sqlite,
                  ControllerSubFunc.db_select, param3);

              var param4 = [
                "SELECT * FROM player where status = ?",
                [0],
                {"calledMethod": 'activePlayer'}
              ];

              controller.execFunction(ControllerFunc.db_sqlite,
                  ControllerSubFunc.db_select, param4);
            }

            //only active player data---------------------
            else if (response["calledMethod"] == "activePlayer") {
              setState(() {
                playerdatastatus = response['response_data'].length;
              });

              var param3 = [
                "SELECT * FROM stadium  where status = ?",
                [0],
                {"calledMethod": 'activeStadium'}
              ];

              controller.execFunction(ControllerFunc.db_sqlite,
                  ControllerSubFunc.db_select, param3);
            }
            //only active stadium data ----------------
            else if (response["calledMethod"] == "activeStadium") {
              setState(() {
                stadiumdatastatus = response['response_data'].length;
                _loading = false;
              });
            } else if (response["calledMethod"] == "result") {
              await setResultData(response['response_data']);

              setState(() {
                resultDatastatus = response['response_data'].length;
              });
              var param3 = [
                "SELECT * FROM stadium",
                [],
                {"calledMethod": 'stadium'}
              ];

              controller.execFunction(ControllerFunc.db_sqlite,
                  ControllerSubFunc.db_select, param3);
            }
            //all stadium data----------------------
            else if (response["calledMethod"] == "stadium") {
              var param3 = [
                "SELECT * FROM scoreData where setFinished = ? AND type != ?",
                [1, 'FAULT'],
                {"calledMethod": 'scoreData'}
              ];

              controller.execFunction(ControllerFunc.db_sqlite,
                  ControllerSubFunc.db_select, param3);
            } else if (response["calledMethod"] == "scoreData") {
              await setScoreSetData(response['response_data']);
              setDataList();

              setState(() {
                scoreatastatus = response['response_data'].length;
              });
            } else if (response["calledMethod"] == "GameSettingsEnd") {
              setState(() {
                gameSettingsEnd = response['response_data'].length;
              });
              await setPauseData(response['response_data'], "GameSettingsEnd");

              var param3 = [
                "SELECT * FROM scoreData where match_id=? ORDER BY id DESC LIMIT 1",
                [matchId],
                {"calledMethod": 'scoreDataEnd'}
              ];
              controller.execFunction(ControllerFunc.db_sqlite,ControllerSubFunc.db_select, param3);

            } else if (response["calledMethod"] == "scoreDataEnd") {
              await setPauseData(response['response_data'], "scoreDataEnd");

               var param4 = [
                "SELECT * FROM scoreData where setFinished = ? and match_id= ? ",
                [1, matchId],
                {
                  "calledMethod":'score1DataEnd'
                }
              ];

              controller.execFunction(ControllerFunc.db_sqlite,ControllerSubFunc.db_select,param4);
            } else if (response["calledMethod"] == "score1DataEnd") {
              await setPauseData(response['response_data'], "score1DataEnd");
            }
            //delete history data-----------------------
          } else if (subFunc == ControllerSubFunc.db_select_batch) {
            if (response["calledMethod"] == "deleteAllData") {

              setState(() {
                _listOfHistory = [];
                _listGame = [];
                _listPlayer = [];
                _listResult = [];
                _listScoreData = [];
                matchIds = [];
              });
              _loadData();

            }
          } 

          break;
        }
      default:
    }
  }

  Widget _buildList(BuildContext context, Axis direction) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: ListView.builder(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 75),
          scrollDirection: direction,
          itemBuilder: (context, index) {
            final Axis slidableDirection =
                direction == Axis.horizontal ? Axis.vertical : Axis.horizontal;
            var historyList = _listOfHistory[index];
            return _getSlidableWithLists(context, index, slidableDirection);
          },
          itemCount: _listOfHistory.length,
        ));
  }

  Widget _getSlidableWithLists(
      BuildContext context, int index, Axis direction) {
    final Widget historyList = _listOfHistory[index];
    return Slidable(
      // key: Key(historyList.city.toString()),
      controller: slidableController,
      direction: direction,
      actionPane: SlidableBehindActionPane(),
      actionExtentRatio: 0.25,
      child: VerticalListItem(_listOfHistory[index]),
      secondaryActions: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 8),
          child: IconSlideAction(
              iconWidget: new IconTheme(
                data: new IconThemeData(color: Colors.red, size: 50),
                child: new Icon(Icons.delete),
              ),
              caption: 'Delete',
              foregroundColor: AppStyle.black,
              color: AppStyle.secondary_color,
              onTap: () {
                var matchData;
                matchData = matchIdArray[index];
                var gameSt =
                    _listResult[index].gameStatus.toString(); //game status

                showDialog(
                    context: context,
                    builder: (context) => CommonPopup(
                        context, "delete", [matchData, gameSt], deleteAllData));
              }),
        ),
      ],
    );
  }
}

class VerticalListItem extends StatelessWidget {
  VerticalListItem(this._listOfHistory);
  final Widget _listOfHistory;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          Slidable.of(context)?.renderingMode == SlidableRenderingMode.none
              ? Slidable.of(context)?.open()
              : Slidable.of(context)?.close(),
      child: Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0), child: _listOfHistory),
    );
  }
}
