import 'dart:io';

// import 'package:tenizo/custom/commom_popup.dart';
import 'package:tenizo/model/color_loader.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tenizo/util/popup_without_pop.dart';

class Players extends StatefulWidget {
  final Function onSelected;
  final Function onArgumentSaving;
  final String userName;
  @override
  _Players createState() => _Players();
  Players({Key key, this.onSelected, this.onArgumentSaving, this.userName})
      : super(key: key);
}

class _Players extends State<Players> with BaseControllerListner {
  BaseController controller;
  // FOR DISPLAYING THE RACKET ICON TO DENOTE THE KEY PLAYER -----------------------------------------------------
  final List<Widget> _racket = <Widget>[];

  bool _loading = true;

  @override
  void initState() {
    super.initState();
    _racket.add(
      SvgPicture.asset('images/tenisracket.svg'),
    );

    controller = new BaseController(this);
    _loadData();
  }

  _loadData() {
    setState(() {
      value = 0;
    });

    var param1 = [
      "SELECT * FROM player where status = ? and user_name = ?",
      [0, widget.userName]
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
  }

  static var datastatus = 0;
  static var value = 0;
  static var nearFloatingButton = 0;

// DELETE THE PLAYER FROM THE PLAYER CATALOGUE AND DO NOT DELETE FROM THE SAVED DATA--------------------------------------

  deletePlayer(List<String> paramList) {
    String pId = paramList[0];
    setState(() {
      value = 1;
    });
    _deleteDirectoryImage(paramList[1]); //delete image from driectory

    var param = [
      "UPDATE player SET status = ? where player_id = ?",
      [1, pId]
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_update, param);
  }

  // if file not found set default
  List<Widget> _listOfExpansions = [];
  ImageProvider getImageProvider(File f) {
    return f.existsSync() ? FileImage(f) : const AssetImage("images/user.png");
  }

  //Player View/remove/edit popup functions
  _popUpActions(choice, i) async {
    if (choice == "View") {
      widget.onArgumentSaving({
        'pnumber': i['Player_no'],
        'pname': i['name'],
        'pgender': i['gender'],
        'pdateofbirth': i['date_of_birth'],
        'phandedness': i['handedness'],
        'pplaystyle': i['playstyle'],
        'pteam': i['team'],
        'prole': i['role'],
        'pplayerimage': i['player_image']
      });
      widget.onSelected(RoutingData.PlayerProfile, false, true);
      // Navigator.of(context).pop();
    } else if (choice == "Edit") {
      widget.onArgumentSaving({
        'pid': i['player_id'],
        'pnumber': i['Player_no'],
        'pname': i['name'],
        'pgender': i['gender'],
        'pdateofbirth': i['date_of_birth'],
        'phandedness': i['handedness'],
        'pplaystyle': i['playstyle'],
        'pteam': i['team'],
        'prole': i['role'],
        'pplayerimage': i['player_image']
      });
      widget.onSelected(RoutingData.PlayerEdit, false, true);
      // Navigator.of(context).pop();
    } else if (choice == "Delete") {
      showDialog(
          context: context,
          builder: (context) => CustomAppPopUpForDelete(context, "Delete",
              [i['player_id'], i['player_image']], deletePlayer));
    }
  }

//delete image from directory
  _deleteDirectoryImage(playerImage) async {
    if (playerImage != null) {
      final delFir = File(playerImage);
      if (delFir.existsSync()) {
        delFir.deleteSync(recursive: true); //for delete
      }
    }
  }

  setList(data) async {
    data.forEach((i) {
      String playerImg = i['player_image'];

      setState(() {
        _listOfExpansions.add(
          Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 12),
                decoration: BoxDecoration(
                  //SETTING THE BACKGROUND COLOR OF THE ROW -------------------------------------------

                  color: AppColors.white,
                  border: Border(
                    // Bottom line ---------
                    bottom: BorderSide(
                      color: AppColors.secondary_color,
                      width: 1.2,
                    ),
                  ),
                ),
                child: Row(children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(12, 8, 0, 0),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: profileWidth,
                              height: profileHeigh,
                              padding: EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: AppColors.primary_color,
                                  width: 2,
                                ),
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: playerImg != null
                                      ? getImageProvider(File(playerImg))
                                      : new AssetImage("images/user.png"),
                                ),
                              ),
                            ),
                            Positioned(
                                bottom: -4,
                                right: 5,
                                child: i['role'] == 'Key'
                                    ? Container(
                                        width: 30,
                                        height: 30,
                                        padding: EdgeInsets.all(4.0),
                                        child: GridView.extent(
                                          shrinkWrap: true,
                                          maxCrossAxisExtent: 50,
                                          children: _racket.toList(),
                                        ))
                                    : new Container()),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                    ),
                    Container(
                      width: currentWidth - 150,
                      padding: EdgeInsets.only(left: 20.0),
                      child: new Text(
                        i['Player_no'] < 10
                            ? "0" +
                                i['Player_no'].toString() +
                                " - " +
                                i['name']
                            : i['Player_no'].toString() + " - " + i['name'],
                        style: TextStyle(
                            fontSize: 20,
                            color: AppColors.primary_color,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Rajdhani'),
                            // overflow: TextOverflow.ellipsis,
                            overflow: TextOverflow.fade,
                             maxLines: 1,
                              softWrap: false,
                      ),
                    ),
                  ]),
                  Expanded(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                        PopupMenuButton(
                          onSelected: (choice) {
                            _popUpActions(choice, i);
                          },
                          child: Container(
                            alignment: Alignment.topRight,
                            child: Icon(
                              Icons.more_vert,
                              size: 35,
                              color: AppColors.primary_color,
                            ),
                          ),
                          itemBuilder: (context) => <PopupMenuItem<String>>[
                            PopupMenuItem<String>(
                              value: 'View',
                              child: SizedBox.expand(
                                  child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(
                                      Icons.visibility,
                                      size: 28,
                                    ),
                                  ),
                                  Text(
                                    'View',
                                    style: TextStyle(
                                      fontSize: 20,
                                    ),
                                  ),
                                ],
                              )),
                            ),
                            PopupMenuItem<String>(
                              value: 'Edit',
                              child: SizedBox.expand(
                                  child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(
                                      Icons.edit,
                                      size: 28,
                                    ),
                                  ),
                                  Text(
                                    'Edit',
                                    style: TextStyle(fontSize: 20),
                                  ),
                                ],
                              )),
                            ),
                            PopupMenuItem<String>(
                                value: 'Delete',
                                child: SizedBox.expand(
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Icon(
                                          Icons.delete,
                                          size: 28,
                                        ),
                                      ),
                                      Text(
                                        'Delete',
                                        style: TextStyle(fontSize: 20),
                                      ),
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ]))
                ]),
              )),
        );
      });
    });

    _listOfExpansions.add(
      Container(
        alignment: Alignment(1.0, 1.0),
        height: 86,
        color: AppColors.white,
      ),
    );
  }

  static var deviceWidth = 0.0;
  static var currentWidth = 0.0;
  static var deviceHeight = 0.0;
  static var currentHeight = 0.0;
  var paddingData;
  var paddingData3;
  var paddingData2 = EdgeInsets.only(
      right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 8);

  var buttonWidth = 60.0;
  var buttonHeight = 60.0;
  var iconSize = 40.0;

  var profileWidth = 70.0;
  var profileHeigh = 70.0;

  var playerBTNWidth = 70.0;
  var playerBTNHeigh = 70.0;
  var playerBTNicon = 60.0;

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 8);
      setState(() {
        currentWidth = 300;
      });
    } else if (deviceWidth <= 450) {
      currentWidth = 380;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 8);
    } else if (deviceWidth <= 550) {
      currentWidth = 450;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 9);
    } else if (deviceWidth <= 750) {
      currentWidth = 450;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 9);
    } else if (deviceWidth > 750) {
      currentWidth = 700;
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 11);
    } else {
      paddingData2 = EdgeInsets.only(
          right: 30.0, top: 60.0, bottom: 10, left: currentWidth / 12 * 11);
      currentWidth = 600;
    }

    if (deviceHeight <= 500) {
      currentHeight = 300;

      paddingData = const EdgeInsets.only(top: 30.0, left: 190);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      profileWidth = 50;
      profileHeigh = 50;

      playerBTNWidth = 50;
      playerBTNHeigh = 50;
      playerBTNicon = 40;
    } else if ((deviceHeight <= 600)) {
      currentHeight = 350;

      paddingData = const EdgeInsets.only(top: 30.0, left: 170, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      profileWidth = 50;
      profileHeigh = 50;

      playerBTNWidth = 60;
      playerBTNHeigh = 60;
      playerBTNicon = 50;
    } else if ((deviceHeight <= 700)) {
      currentHeight = 400;

      paddingData = const EdgeInsets.only(top: 50.0, left: 240, right: 00.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 6);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      profileWidth = 60;
      profileHeigh = 60;

      playerBTNWidth = 60;
      playerBTNHeigh = 60;
      playerBTNicon = 50;
    } else if ((deviceHeight <= 800)) {
      currentHeight = 400;

      paddingData = const EdgeInsets.only(top: 50.0, left: 300, right: 05.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 6);

      buttonWidth = 60;
      buttonHeight = 60;
      iconSize = 40;

      playerBTNWidth = 60;
      playerBTNHeigh = 60;
      playerBTNicon = 50;
    } else if ((deviceHeight <= 900)) {
      currentHeight = 450;

      paddingData = const EdgeInsets.only(top: 50.0, left: 350, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 70;
      buttonHeight = 70;
      iconSize = 50;
    } else if (deviceHeight <= 1000) {
      currentHeight = 500;

      paddingData = const EdgeInsets.only(top: 50.0, left: 325, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 70;
      buttonHeight = 70;
      iconSize = 50;
    } else if (deviceHeight > 1000) {
      // for tab
      currentHeight = 600;

      paddingData = const EdgeInsets.only(top: 100.0, left: 640, right: 0.0);
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);

      buttonWidth = 70;
      buttonHeight = 70;
      iconSize = 50;
    } else {
      currentHeight = 450;
      paddingData3 = const EdgeInsets.only(top: 20.0, right: 5);
      paddingData = const EdgeInsets.only(top: 50.0, left: 340, right: 0.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: _loading
            ? Container(
                color: Colors.white, child: Center(child: ColorLoader()))
            : LayoutBuilder(
                builder: (ctx, c) {
                  if (datastatus == 0) {
                    return Scaffold(
                      backgroundColor: Color(0xFFE5F9E0),
                      body: SizedBox.expand(
                        child: Container(
                          alignment: Alignment.center,
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 5),
                                  child: Padding(
                                    padding: paddingData3,
                                    child: Container(
                                      width: currentWidth,
                                      height: currentHeight - 180,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                          color: Colors.white,
                                          style: BorderStyle.solid,
                                        ),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "Add Your First Player",
                                          style: TextStyle(
                                              fontSize: 30,
                                              color: AppStyle.color_Head,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: 'Rajdhani'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Padding(
                                      padding: paddingData2,
                                      
                                      child: Container(
                                        // color: Colors.yellow,
                                        width: 50,
                                        height: 60,
                                        decoration: BoxDecoration(
                                          image: new DecorationImage(
                                            fit: BoxFit.fill,
                                            image: new AssetImage(
                                                "images/down_arrow.png"),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: paddingData2,
                                      
                                      child: Container(
                                        width: buttonWidth,
                                        height: buttonHeight,
                                        decoration: BoxDecoration(
                                          color: AppStyle.color_Head,
                                          borderRadius:
                                              BorderRadius.circular(50.0),
                                          border: Border.all(
                                            color: AppStyle.color_Head,
                                            style: BorderStyle.solid,
                                          ),
                                        ),
                                        child: FloatingActionButton(
                                          onPressed: () {
                                            widget.onSelected(
                                                RoutingData.PlayerRegistration,
                                                false,
                                                true);
                                          },
                                          isExtended: true,
                                          materialTapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          backgroundColor: AppStyle.color_Head,
                                          child: Icon(
                                            Icons.add,
                                            size: iconSize,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return Scaffold(
                      //FLOATING BUTTON -----------------------------------------------------------------
                      backgroundColor: AppColors.white,

                      floatingActionButton: Container(
                        width: playerBTNWidth,
                        height: playerBTNHeigh,
                        child: FloatingActionButton(
                          onPressed: () {
                            widget.onSelected(
                                RoutingData.PlayerRegistration, false, true);
                          },
                          isExtended: true,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          backgroundColor: AppStyle.color_Head,
                          child: Icon(
                            Icons.add,
                            size: playerBTNicon,
                          ),
                        ),
                      ),

                      //BODY---------------------------------------------------------------
                      body: Column(
                        children: <Widget>[
                          Expanded(
                            child: ListView(
                              padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                              children: _listOfExpansions
                                  .map((card) => card)
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
    if (subFunc == ControllerSubFunc.db_select && value == 0) {
      setList(response['response_data']);
      setState(() {
        datastatus = response['response_data'].length;
        _loading = false;
      });
    } else {
      Navigator.of(context).pop();
      setState(() {
        _listOfExpansions = [];
      });

      _loadData();
    }
  }
}
