import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../tennizo_controller_functions.dart';
import '../const.dart';

class Stats extends StatefulWidget {
  final arguments;
  final Function onSelected;
  final Function onArgumentSaving;
  final Function screenShot;
  final GlobalKey screen;
  final initAd;
  final showAd;

  @override
  _StatsState createState() => _StatsState();
  Stats(
      {Key key,
      this.onSelected,
      this.onArgumentSaving,
      this.arguments,
      this.screenShot,
      this.screen,
      this.initAd,
      this.showAd})
      : super(key: key);
}

class _StatsState extends State<Stats> with BaseControllerListner {
  BaseController controller;

  String gameId;

  static const balck_color = const Color(0xDD000000);
  static const pageContainer_bgcolor = const Color(0xFFFFFFFF);
  int count = 0;

  double twoPlayerRowHeight = 100.0;
  double secondRowHeight = 30.0;
  double tabButtonHeight = 40.0;
  double gameDeatilsContainerHeight = 50.0;
  double expansionTileHeight = 30.0;
  //sizeboxheight
  double sizeBoxSmall = 10.0;
  double sizeBoxScoreSpace = 15.0;
  double sizeBoxMeadium = 30.0;

  double gameDeatilsMiddleContainerWidth = 150.0;
  double subContainerWidth = 80.0;

  double profilePicPaddingH = 10.0;
  double profilePicPaddingV = 10.0;
  double scoreBoardPadding = 10.0;
  double gameDetailsContainerH = 5.0;
  double firstRwoBottmPadding = 0.0;
  double scoreTableContainerPadding = 20.0;
  double scoreTablePadding = 10.0;
  double winnerRowPadding = 0.0;
  double uErrRowPadding = 0.0;
  double fErrRowPadding = 0.0;

  //font
  double palyerNameFont = 20.0;
  double dateTimeFont = 15.0;
  double scoreDisplayFont = 25.0;
  double tabButtonFont = 25.0;
  double tableDataFont = 18.0;
  double tableTitleFont = 20.0;
  double gameDeatilsContainerFont = 18.0;
  double gameDeatilsSubContainerFont = 18.0;
  double gameDeatilsContaineFont = 12.0;
  double gameDeatilsContainerTitleFont = 15.0;
  double dropdowonPadding = 100.0;

  String player1No = "";
  String player2No = "";
  String player1Name = " ";
  String player2Name = " ";
  List<String> playerImage = ["", ""];
  String points = "";
  String date = " ";
  String time = " ";
  String player1AceCount = " ";
  String player2AceCount = " ";
  String imageName = " ";
  String advantage = '';

  static List<int> temp;
  List<int> gameWinPoint = [0, 0];
  List<int> timeBreakWinPoint = [0, 0];
  List<int> winPoint = [0, 0];
  List<int> breakPoint = [0, 0];
  List<int> backHandWinPoint = [0, 0];
  List<int> backHandVolleyWinPoint = [0, 0];
  List<int> foreHandWinPoint = [0, 0];
  List<int> forHandVolleyWinPoint = [0, 0];
  List<int> smashWinPoint = [0, 0];
  List<int> point = [0, 0];
  List<int> aceCount = [0, 0];
  List<int> fltCount = [0, 0];
  List<int> dFltCount = [0, 0];
  List<int> fErrCount = [0, 0];
  List<int> uErr = [0, 0];
  List<int> firstServicePointWon = [0, 0];
  List<int> secondServicePointWon = [0, 0];
  List<int> servicePointWon = [0, 0];
  List<int> serviceGametWon = [0, 0];
  List<int> reciverPoints = [0, 0];
  List<int> reciveGameWon = [0, 0];
  List<int> firstReciverPoints = [0, 0];
  List<int> secondReciverPoints = [0, 0];

  List<int> maxGameWonInRow = [1, 1];
  List<int> maxGamePointInRow = [1, 1];

  List<int> uErrBackHand = [0, 0];
  List<int> uErrBackHandVolley = [0, 0];
  List<int> uErrForeHand = [0, 0];
  List<int> uErrForHandVolley = [0, 0];
  List<int> uErrSmash = [0, 0];

  List<int> fErrBackHand = [0, 0];
  List<int> fErrBackHandVolley = [0, 0];
  List<int> fErrForeHand = [0, 0];
  List<int> fErrForHandVolley = [0, 0];
  List<int> fErrSmash = [0, 0];

  List<String> backHandWinPointString = ["", ""];
  List<String> backHandVolleyWinPointString = ["", ""];
  List<String> foreHandWinPointString = ["", ""];
  List<String> forHandVolleyWinPointString = ["", ""];
  List<String> smashWinPointString = ["", ""];

  List<String> firstServiceInString = ["", ""];
  List<String> firstServicePointWonString = ["", ""];
  List<String> secondServicePointWonString = ["", ""];
  List<String> firstReciverPointsString = ["", ""];
  List<String> secondReciverPointsString = ["", ""];

  List<String> uErrBackHandString = ["", ""];
  List<String> uErrBackHandVolleyString = ["", ""];
  List<String> uErrForeHandString = ["", ""];
  List<String> uErrForHandVolleyString = ["", ""];
  List<String> uErrSmashString = ["", ""];

  List<String> fErrBackHandString = ["", ""];
  List<String> fErrBackHandVolleyString = ["", ""];
  List<String> fErrForeHandString = ["", ""];
  List<String> fErrForHandVolleyString = ["", ""];
  List<String> fErrSmashString = ["", ""];

  static List<String> player1set = ["", "", "", "", ""];
  static List<String> player2set = ["", "", "", "", ""];
  static List<String> tieBreakOnListPlayer1 = ["", "", "", "", ""];
  static List<String> tieBreakOnListPlayer2 = ["", "", "", "", ""];

  static List<String> tabelNameContainer = ["1ST", "2ND", "3RD", "4TH", "5TH"];
  static List<String> tabelName = ["", "", "", "", ""];

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;

  var arrowIcon = Icons.keyboard_arrow_right;
  var arrowIconWinner = Icons.keyboard_arrow_right;
  var arrowIconFError = Icons.keyboard_arrow_right;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
    setState(() {
      date = widget.arguments["date"].toString();
    });

    gameId = widget.arguments["matchID"].toString();

    playername(widget.arguments["player1"], "playerOne");
    playername(widget.arguments["player2"], "playerTwo");

    gameDate();
    getresultData();

    // if (Const.buttonCount == 1) {
    //   widget.showAd(0);
    // }
    
  }

  //caluculate game win point
  void caluculateGameWinPint() {

    for (count = 0; count < player1set.length; count++) {
      if (player1set[count] != "") {
        
        setState(() {

          //win point set
          gameWinPoint[0] += int.parse(player1set[count]);
          gameWinPoint[1] += int.parse(player2set[count]);

        });

      }
    }
  }

  //get game date
  void gameDate(){
    var selectQuery = [
      "SELECT * FROM gameSettings where match_id = ?",
      [gameId],
      {"calledMethod": "gameSettings"}
    ];
    controller.execFunction(ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  //Display timebreak values caluculate timebreak point
  void timeBreakValues() {
    
    for (count = 0; count < tieBreakOnListPlayer1.length; count++) {
      setState(() {
        if (tieBreakOnListPlayer1[count] != "" || tieBreakOnListPlayer1[count] != "") {
          
          if (int.parse(tieBreakOnListPlayer1[count])  > int.parse(tieBreakOnListPlayer2[count])) {
           timeBreakWinPoint[0]++;            
          } 
          else {
            timeBreakWinPoint[1]++;       
          }

          tieBreakOnListPlayer1[count] = "["+tieBreakOnListPlayer1[count]+"]";
          tieBreakOnListPlayer2[count] = "["+tieBreakOnListPlayer2[count]+"]";

        }
      });
    }
  }

  // get name of two players
  void playername(var id, var type) {
    var selectQuery = [
      "SELECT * FROM player where player_id = ?",
      [id],
      {"calledMethod": type}
    ];
    controller.execFunction(ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  //get winner count of two palyers......................................
  void getWinnerCount() {
    var selectQuery = [
      "SELECT * FROM scoreData where match_id = ?",

      [gameId],
      {"calledMethod": "getScoreData"}
    ];
    controller.execFunction(ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  //get get result Data data count of two palyers......................................
  void getresultData() {
    var selectQuery = [
      "SELECT * FROM result where match_id = ?",
      [gameId],
      {"calledMethod": "getresultData"}
    ];
    controller.execFunction(ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  //convenrt seconds into hours
  void timeConvert(int sec) {
    int h = (sec / 3600).round();
    sec = (sec % 3600).round();
    int min = (sec / 60).round();
    sec = (sec % 60).round();

    setState(() {
      time = h.toString() + ":" + min.toString() + ":" + sec.toString();
    });
  }

  palyerNameDisplay() {
    return TextStyle(
        fontSize: palyerNameFont,
        color: AppColors.primary_color,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  scoreDisplay() {
    return TextStyle(
        fontSize: scoreDisplayFont,
        color: AppColors.black,
        fontWeight: FontWeight.w800,
        fontFamily: 'Rajdhani');
  }

  tabButtonDisplay() {
    return TextStyle(
        fontSize: tabButtonFont,
        color: balck_color,
        fontWeight: FontWeight.w600,
        fontFamily: 'Rajdhani');
  }

  dateTimeDisplay() {
    return TextStyle(
        fontSize: dateTimeFont,
        color: balck_color,
        fontWeight: FontWeight.w600,
        fontFamily: 'Rajdhani');
  }

  tableDataDisaplay() {
    return TextStyle(
        fontSize: tableDataFont,
        color: balck_color,
        fontWeight: FontWeight.w600,
        fontFamily: 'Rajdhani');
  }

  tableTitleDisaplay() {
    return TextStyle(
        fontSize: tableTitleFont,
        color: AppColors.primary_color,
        fontWeight: FontWeight.w600,
        fontFamily: 'Rajdhani');
  }

  gameDeatilsContainerTitle() {
    return TextStyle(
        fontSize: gameDeatilsContainerTitleFont,
        color: AppColors.primary_color,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  gameDeatilsSubContainerTitle() {
    return TextStyle(
        fontSize: gameDeatilsContainerTitleFont,
        color: AppColors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  gameDeatilsContainerData() {
    return TextStyle(
        fontSize: gameDeatilsContaineFont,
        color: AppColors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  _setDeviceWidth() {
    //print("deviceWidth "+deviceWidth.toString());
    if (deviceWidth <= 350) {
      //dropdowonPadding = 100.0;
      subContainerWidth = 80.0;
      twoPlayerRowHeight = 70.0;
    } else if (deviceWidth <= 440) {
      //print("440");
      subContainerWidth = 120.0;
      profilePicPaddingH = 20.0;
      profilePicPaddingV = 10.0;
      gameDeatilsContaineFont = 15.0;
      gameDeatilsContainerTitleFont = 18.0;
      expansionTileHeight = 35.0;
      gameDetailsContainerH = 15.0;
    } else {
      subContainerWidth = 130.0;
      winnerRowPadding = 0.0;
      uErrRowPadding = 5.0;
      fErrRowPadding = 5.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();
    return WillPopScope(
      child: ListView(
        children: <Widget>[
          MediaQuery(
            data: MediaQueryData(),
            child: Container(
              height: MediaQuery.of(context).size.height,
              color: Color(0xFFE5F9E0),
              child: Center(
                child: Column(
                  children: <Widget>[
                    //flow state button column
                    Container(
                      height: 50,
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            border: Border.all(
                              color: AppColors.black,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: GestureDetector(
                                  child: Container(
                                    width: double.infinity / 2,
                                    decoration: BoxDecoration(
                                      border: Border(right: BorderSide(width: 1.0, color: Colors.black),),
                                    ),
                                    height: 50,
                                    child: Center(
                                      child: Text(
                                        "Flow",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 22.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    widget.onArgumentSaving({
                                      "matchID": gameId,
                                      "player1": widget.arguments["player1"],
                                      "player2": widget.arguments["player2"],
                                      "time": widget.arguments["time"],

                                    });
                                    widget.onSelected(RoutingData.Flow, false, true);
                                  },
                                ),
                              ),
                              Expanded(
                                child: GestureDetector(
                                  child: Container(
                                    width: double.infinity / 2,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: AppColors.ternary_color,
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(5.0),
                                        bottomRight: Radius.circular(5.0),
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Stats",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 22.0,fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    widget.onSelected(
                                        RoutingData.Stats, false, true);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    //page body
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 6.0),
                      child: Container(
                        height: MediaQuery.of(context).size.height - 140,
                        decoration: BoxDecoration(
                          color: pageContainer_bgcolor,
                          border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Scrollbar(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Column(
                              children: <Widget>[
                                RepaintBoundary(
                                  key: widget.screen,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 6.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                      color: AppColors.white,
                                      ),
                                      child: Column(
                                        children: <Widget>[
                                          SizedBox(
                                            height: sizeBoxSmall,
                                          ),

                                          //first row 2 palyer view------------------------------------------------

                                          Row(
                                            mainAxisAlignment:MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                  height: twoPlayerRowHeight,
                                                  width: twoPlayerRowHeight,
                                                  //decoration: BoxDecoration(color: Colors.red),
                                                  child: Center(
                                                    child: Container(
                                                      decoration:new BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        image: new DecorationImage(
                                                          fit: BoxFit.cover,
                                                          image: playerImage[0] !=null
                                                              ? getImageProvider(File(playerImage[0]))
                                                              : new AssetImage("images/user.png"),
                                                        ),
                                                        border: Border.all(
                                                          color: AppColors
                                                              .primary_color,
                                                          width: 3.0,
                                                        ),
                                                    )),
                                                  )),
                                              Container(
                                                  height: twoPlayerRowHeight,
                                                  width: 125.0,
                                                  //decoration: BoxDecoration(color: Colors.yellow),
                                                  child: Center(
                                                    child: Text(
                                                      "VS",
                                                      style: palyerNameDisplay(),
                                                    ),
                                                  )),
                                              Container(
                                                  height: twoPlayerRowHeight,
                                                  width: twoPlayerRowHeight,
                                                  //decoration: BoxDecoration(color: Colors.red),
                                                  child: Center(
                                                    child: Container(
                                                        decoration:
                                                            new BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      image: new DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: playerImage[1] !=
                                                                null
                                                            ? getImageProvider( File(playerImage[1]))
                                                            : new AssetImage("images/user.png"),
                                                      ),
                                                      border: Border.all(
                                                        color: AppColors
                                                            .primary_color,
                                                        width: 3.0,
                                                      ),
                                                    )),
                                                  )),
                                            ],
                                          ),

                                          //name marks row

                                          Row(
                                            mainAxisAlignment:MainAxisAlignment.center,
                                            children: <Widget>[
                                              //player 1 name
                                              Container(
                                                // height: secondRowHeight,
                                                width: twoPlayerRowHeight,
                                                child: Center(
                                                  child: Text(
                                                    player1No +" - " +player1Name,
                                                    style: palyerNameDisplay(),
                                                   maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ),

                                              //marks
                                              Container(
                                                height: secondRowHeight,
                                                width: 125.0,
                                                child: Center(
                                                  child: Text(
                                                    points,
                                                    style: scoreDisplay(),
                                                  ),
                                                ),
                                              ),

                                              //player 2 name
                                              Container(
                                                width: twoPlayerRowHeight,
                                                child: Center(
                                                  child: Text(
                                                    player2No +" - " +player2Name,
                                                    style: palyerNameDisplay(),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                    
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),

                                          SizedBox(
                                            height: sizeBoxSmall,
                                          ),

                                          //date and time
                                          Row(
                                            children: <Widget>[
                                              //date
                                              Expanded(
                                                child: Container(
                                                  height: secondRowHeight,
                                                  //decoration: BoxDecoration(color: Colors.red),
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.only(left: 10),
                                                    child: Text(
                                                      "Date:" + date,
                                                      style: dateTimeDisplay(),
                                                    ),
                                                  ),
                                                ),
                                              ),

                                              //time
                                              Expanded(
                                                child: Container(
                                                  height: secondRowHeight,
                                                  //decoration: BoxDecoration(color: Colors.green),
                                                  child: Text(
                                                    "Time Duration: " + time,
                                                    style: dateTimeDisplay(),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),

                                          //Score table
                                          Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: scoreBoardPadding),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  top:
                                                      scoreTableContainerPadding),
                                              decoration: BoxDecoration(
                                                color: AppColors.secondary_color,
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        scoreTablePadding),
                                                child: Table(
                                                  columnWidths: {
                                                    0: FractionColumnWidth(.3)
                                                  },
                                                  children: [
                                                    TableRow(children: [
                                                      Center(
                                                          child: Text(
                                                        "",
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      Center(
                                                          child: Text(
                                                        tabelName[0],
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      Center(
                                                          child: Text(
                                                        tabelName[1],
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      Center(
                                                          child: Text(
                                                        tabelName[2],
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      Center(
                                                          child: Text(
                                                        tabelName[3],
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      Center(
                                                          child: Text(
                                                        tabelName[4],
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                    ]),
                                                    TableRow(children: [
                                                      Center(
                                                          child: Text(
                                                        " ",
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      )
                                                    ]),
                                                    TableRow(children: [
                                                      Center(
                                                          child: Text(
                                                        " ",
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      )
                                                    ]),
                                                    TableRow(children: [
                                                      Text(
                                                        player1Name,
                                                        style:
                                                            tableTitleDisaplay(),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                      ),
                                                      Center(
                                                          child: Padding(padding:EdgeInsets.only(bottom:firstRwoBottmPadding /2),
                                                              child: Text(
                                                                player1set[0]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer1[
                                                                            0]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player1set[1]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer1[
                                                                            1]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player1set[2]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer1[
                                                                            2]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player1set[3]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer1[
                                                                            3]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player1set[4]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer1[
                                                                            4]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                    ]),
                                                    TableRow(children: [
                                                      Center(
                                                          child: Text(
                                                        " ",
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      SizedBox(
                                                        height:firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:firstRwoBottmPadding,
                                                      )
                                                    ]),
                                                    TableRow(children: [
                                                      Text(
                                                        player2Name,
                                                        style:
                                                            tableTitleDisaplay(),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        
                                                      ),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player2set[0]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer2[
                                                                            0]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player2set[1]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer2[
                                                                            1]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player2set[2]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer2[
                                                                            2]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player2set[3]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer2[
                                                                            3]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                      Center(
                                                          child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom:
                                                                          firstRwoBottmPadding /
                                                                              2),
                                                              child: Text(
                                                                player2set[4]
                                                                        .toString() +
                                                                    " " +
                                                                    tieBreakOnListPlayer2[
                                                                            4]
                                                                        .toString(),
                                                                style:
                                                                    tableDataDisaplay(),
                                                              ))),
                                                    ]),
                                                    TableRow(children: [
                                                      Center(
                                                          child: Text(
                                                        "",
                                                        style:
                                                            tableTitleDisaplay(),
                                                      )),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      ),
                                                      SizedBox(
                                                        height:
                                                            firstRwoBottmPadding,
                                                      )
                                                    ]),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),

                                          SizedBox(
                                            height: sizeBoxMeadium,
                                          ),

                                          //game details container
                                          Column(
                                            children: <Widget>[
                                              
                                              //Aces
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          aceCount[0].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Aces",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          aceCount[1].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Faults
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          fltCount[0].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Faults",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          fltCount[1].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Double Fault
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          dFltCount[0].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Double Fault",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          dFltCount[1].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Winners drop down list
                                                ExpansionTile(
                                                onExpansionChanged:
                                                    _onExpansionWinner,
                                                trailing: Container(
                                                  width: deviceWidth - 60,
                                                  // color: Colors.red,
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Align(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Container(
                                                          width: 60,
                                                          child: Text(
                                                            winPoint[0].toString(),
                                                            style:
                                                                gameDeatilsContainerData(),
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Container(
                                                          width: 180,
                                                          child: Center(
                                                            child: Container(
                                                              width: 180,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.only(left:30.0),
                                                                      child: Text(
                                                                        "Winners",
                                                                        style:
                                                                            gameDeatilsContainerTitle(),
                                                                        textAlign:
                                                                            TextAlign
                                                                                .center,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            0.0),
                                                                    child: Icon(
                                                                      arrowIconWinner,
                                                                      size: 30,
                                                                      color: AppColors
                                                                          .primary_color,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Container(
                                                          width: 60,
                                                          child: Text(
                                                            winPoint[1].toString(),
                                                            style:
                                                                gameDeatilsContainerData(),
                                                            textAlign:
                                                                TextAlign.right,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                title: null,
                                                children: <Widget>[
                                                  //list
                                                  //BackHand
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              backHandWinPointString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Backhand",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              backHandWinPointString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //backhand volly
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              backHandVolleyWinPointString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Backhand Volly",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              backHandVolleyWinPointString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //forehand volly
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              forHandVolleyWinPointString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Forehand Volly",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              forHandVolleyWinPointString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //forehand
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              foreHandWinPointString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Forehand",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              foreHandWinPointString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //Smash
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              smashWinPointString[
                                                                  0],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Smash",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              smashWinPointString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),

                                              
                                              
                                              // SizedBox(
                                              //   height: 20.0,
                                              // ),

                                              //forced Erros
                                              ExpansionTile(
                                                onExpansionChanged:
                                                    _onExpansionFError,
                                                trailing: Container(
                                                  width: deviceWidth - 60,
                                                  // color: Colors.red,
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Align(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Container(
                                                          width: 60,
                                                          child: Text(
                                                            fErrCount[0]
                                                                .toString(),
                                                            style:
                                                                gameDeatilsContainerData(),
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Container(
                                                          width: 180,
                                                          child: Center(
                                                            child: Container(
                                                              width: 180,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.only(left:30.0),
                                                                      child: Text(
                                                                        "Forced Errors",
                                                                        style:
                                                                            gameDeatilsContainerTitle(),
                                                                        textAlign:
                                                                            TextAlign
                                                                                .center,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            0.0),
                                                                    child: Icon(
                                                                      arrowIconFError,
                                                                      size: 30,
                                                                      color: AppColors
                                                                          .primary_color,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Container(
                                                          width: 60,
                                                          child: Text(
                                                            fErrCount[1]
                                                                .toString(),
                                                            style:
                                                                gameDeatilsContainerData(),
                                                            textAlign:
                                                                TextAlign.right,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                title: null,
                                                children: <Widget>[
                                                  //list
                                                  //BackHand
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              fErrBackHandString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Backhand",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              fErrBackHandString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //backhand volly
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              fErrBackHandVolleyString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Backhand Volly",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              fErrBackHandVolleyString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //forehand volly
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              fErrForHandVolleyString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Forehand Volly",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              fErrForHandVolleyString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //forehand
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              fErrForeHandString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Forehand",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              fErrForeHandString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //Smash
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              fErrSmashString[
                                                                  0],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Smash",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              fErrSmashString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),

                                              //Unforced Erros
                                               ExpansionTile(
                                                onExpansionChanged:
                                                    _onExpansionError,
                                                trailing: Container(
                                                  width: deviceWidth - 60,
                                                  // color: Colors.red,
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Align(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Container(
                                                          width: 60,
                                                          child: Text(
                                                            uErr[0].toString(),
                                                            style:
                                                                gameDeatilsContainerData(),
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Container(
                                                          width: 180,
                                                          child: Center(
                                                            child: Container(
                                                              width: 180,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.only(left: 30.0),
                                                                      child: Text(
                                                                        "Unforced Errors",
                                                                        style:
                                                                            gameDeatilsContainerTitle(),
                                                                        textAlign:
                                                                            TextAlign
                                                                                .center,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            0.0),
                                                                    child: Icon(
                                                                      arrowIcon,
                                                                      size: 30,
                                                                      color: AppColors
                                                                          .primary_color,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Container(
                                                          width: 60,
                                                          child: Text(
                                                            uErr[1].toString(),
                                                            style:
                                                                gameDeatilsContainerData(),
                                                            textAlign:
                                                                TextAlign.right,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                title: null,
                                                children: <Widget>[
                                                  //list
                                                  //BackHand
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              uErrBackHandString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Backhand",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              uErrBackHandString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //backhand volly
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              uErrBackHandVolleyString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Backhand Volly",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              uErrBackHandVolleyString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //forehand volly
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              uErrForHandVolleyString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Forehand Volly",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              uErrForHandVolleyString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //forehand
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              uErrForeHandString[
                                                                      0]
                                                                  .toString(),
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Forehand",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              uErrForeHandString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  //Smash
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20.0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            child: Text(
                                                              uErrSmashString[
                                                                  0],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height:
                                                              gameDeatilsContainerHeight,
                                                          width:
                                                              gameDeatilsMiddleContainerWidth,
                                                          child: Text(
                                                            "Smash",
                                                            style:
                                                                gameDeatilsSubContainerTitle(),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            height:
                                                                gameDeatilsContainerHeight,
                                                            alignment: Alignment
                                                                .topRight,
                                                            child: Text(
                                                              uErrSmashString[
                                                                  1],
                                                              style:
                                                                  gameDeatilsContainerData(),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),


                                              SizedBox(
                                                height: 20.0,
                                              ), 

                                              //First service In
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        child: Text(
                                                          firstServiceInString[0].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:gameDeatilsContainerHeight,
                                                      width:gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "First Serve In",
                                                        style:gameDeatilsContainerTitle(),
                                                        textAlign:TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        alignment:Alignment.topRight,
                                                        child: Text(
                                                          firstServiceInString[1].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),                                           

                                              //Service Points Won
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          servicePointWon[0].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Service Points Won",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          servicePointWon[1]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //First Serve
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        //decoration: BoxDecoration(color: Colors.green),
                                                        child: Text(
                                                          firstServicePointWonString[0],
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      //decoration: BoxDecoration(color: Colors.red),
                                                      child: Text(
                                                        "First Service Point Won",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          firstServicePointWonString[
                                                              1],
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              
                                              SizedBox(
                                                height: sizeBoxScoreSpace,
                                              ),

                                              //first serve point
                                              // Padding(
                                              //   padding: EdgeInsets.symmetric(
                                              //       horizontal:
                                              //           gameDetailsContainerH),
                                              //   child: Row(
                                              //     children: <Widget>[
                                              //       Expanded(
                                              //         child: Container(
                                              //           height:
                                              //               gameDeatilsContainerHeight,
                                              //           child: Text(
                                              //             firstServicePointWon[0]
                                              //                 .toString(),
                                              //             style:
                                              //                 gameDeatilsContainerData(),
                                              //           ),
                                              //         ),
                                              //       ),
                                              //       Container(
                                              //         height:
                                              //             gameDeatilsContainerHeight,
                                              //         width:
                                              //             gameDeatilsMiddleContainerWidth,
                                              //         child: Text(
                                              //           "First Serve Points",
                                              //           style:
                                              //               gameDeatilsContainerTitle(),
                                              //           textAlign:
                                              //               TextAlign.center,
                                              //         ),
                                              //       ),
                                              //       Expanded(
                                              //         child: Container(
                                              //           height:
                                              //               gameDeatilsContainerHeight,
                                              //           alignment:
                                              //               Alignment.topRight,
                                              //           child: Text(
                                              //             firstServicePointWon[1]
                                              //                 .toString(),
                                              //             style:
                                              //                 gameDeatilsContainerData(),
                                              //           ),
                                              //         ),
                                              //       ),
                                              //     ],
                                              //   ),
                                              // ),

                                              //Second Serve
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(secondServicePointWonString[0],
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:gameDeatilsContainerHeight,
                                                      width:gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Second Service Point Won",
                                                        style:gameDeatilsContainerTitle(),
                                                        textAlign:TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        alignment:Alignment.topRight,
                                                        child: Text(
                                                          secondServicePointWonString[1],
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              SizedBox(
                                                height: sizeBoxScoreSpace,
                                              ),                                            

                                              // SizedBox(
                                              //   height: sizeBoxScoreSpace,
                                              // ),
                                              //Second serve point
                                              // Padding(
                                              //   padding: EdgeInsets.symmetric(
                                              //       horizontal:
                                              //           gameDetailsContainerH),
                                              //   child: Row(
                                              //     children: <Widget>[
                                              //       Expanded(
                                              //         child: Container(
                                              //           height:
                                              //               gameDeatilsContainerHeight,
                                              //           child: Text(
                                              //             secondServicePointWon[0]
                                              //                 .toString(),
                                              //             style:
                                              //                 gameDeatilsContainerData(),
                                              //           ),
                                              //         ),
                                              //       ),
                                              //       Container(
                                              //         height:
                                              //             gameDeatilsContainerHeight,
                                              //         width:
                                              //             gameDeatilsMiddleContainerWidth,
                                              //         child: Text(
                                              //           "Second Serve Points",
                                              //           style:
                                              //               gameDeatilsContainerTitle(),
                                              //           textAlign:
                                              //               TextAlign.center,
                                              //         ),
                                              //       ),
                                              //       Expanded(
                                              //         child: Container(
                                              //           height:
                                              //               gameDeatilsContainerHeight,
                                              //           alignment:
                                              //               Alignment.topRight,
                                              //           child: Text(
                                              //             secondServicePointWon[1]
                                              //                 .toString(),
                                              //             style:
                                              //                 gameDeatilsContainerData(),
                                              //           ),
                                              //         ),
                                              //       ),
                                              //     ],
                                              //   ),
                                              // ),

                                              //Reciver points won
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        child: Text(
                                                          reciverPoints[0].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:gameDeatilsContainerHeight,
                                                      width:gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Receiving Point Won",
                                                        style:gameDeatilsContainerTitle(),
                                                        textAlign:TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        alignment:Alignment.topRight,
                                                        child: Text(
                                                          reciverPoints[1].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //1st server Reciver points won
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        child: Text(
                                                          firstReciverPointsString[0].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: gameDeatilsContainerHeight,
                                                      width:gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "First Recive Point Won",
                                                        style:gameDeatilsContainerTitle(),
                                                        textAlign:TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        alignment:Alignment.topRight,
                                                        child: Text(firstReciverPointsString[1].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              SizedBox(
                                                height: sizeBoxScoreSpace,
                                              ),

                                              //2nd server Reciver points won
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        child: Text(
                                                          secondReciverPointsString[0].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:gameDeatilsContainerHeight,
                                                      width:gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Second Recive Point Won",
                                                        style:gameDeatilsContainerTitle(),
                                                        textAlign:TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        alignment:Alignment.topRight,
                                                        child: Text(
                                                          secondReciverPointsString[1].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              SizedBox(
                                                height: sizeBoxScoreSpace,
                                              ),

                                              //Break point
                                              Padding(
                                                padding:EdgeInsets.symmetric(horizontal: gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height: gameDeatilsContainerHeight,
                                                        child: Text(
                                                          reciveGameWon[0].toString()+"/"+breakPoint[0].toString(),
                                                          style: gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: gameDeatilsContainerHeight,
                                                      width:gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Break Point Won",
                                                        style: gameDeatilsContainerTitle(),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height: gameDeatilsContainerHeight,
                                                        alignment: Alignment.topRight,
                                                        child: Text(
                                                          reciveGameWon[1].toString()+"/"+breakPoint[1].toString(),
                                                          style: gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Tie breaks won
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          timeBreakWinPoint[0]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "TieBreaks Won",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          timeBreakWinPoint[1]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Game won
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          gameWinPoint[0].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Game Won",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          gameWinPoint[1].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Point Won
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          point[0].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Points Won",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          point[1].toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //Max Games Won in a Row
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          maxGameWonInRow[0]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Max Games Won in a Row",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          maxGameWonInRow[1]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              SizedBox(height: sizeBoxScoreSpace),

                                              //Max Points Won in a Row
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        child: Text(
                                                          maxGamePointInRow[0]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:
                                                          gameDeatilsContainerHeight,
                                                      width:
                                                          gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Max Points Won in a Row",
                                                        style:
                                                            gameDeatilsContainerTitle(),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:
                                                            gameDeatilsContainerHeight,
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Text(
                                                          maxGamePointInRow[1]
                                                              .toString(),
                                                          style:
                                                              gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                          
                                              SizedBox(height: sizeBoxScoreSpace),

                                              //Service Games Won
                                              Padding(
                                                padding: EdgeInsets.symmetric(horizontal:gameDetailsContainerH),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        child: Text(serviceGametWon[0].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      height:gameDeatilsContainerHeight,
                                                      width: gameDeatilsMiddleContainerWidth,
                                                      child: Text(
                                                        "Service Games Won",
                                                        style:gameDeatilsContainerTitle(),
                                                        textAlign:TextAlign.center,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        height:gameDeatilsContainerHeight,
                                                        alignment:Alignment.topRight,
                                                        child: Text(serviceGametWon[1].toString(),
                                                          style:gameDeatilsContainerData(),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //SizedBox(height: sizeBoxScoreSpace),      

                                              SizedBox(
                                                height: 50.0,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  void _onExpansionWinner(bool value) {
    setState(() {
      if (arrowIconWinner == Icons.keyboard_arrow_down) {
        arrowIconWinner = Icons.keyboard_arrow_right;
      } else {
        arrowIconWinner = Icons.keyboard_arrow_down;
      }
    });
  }

  void _onExpansionError(bool value) {
    setState(() {
      if (arrowIcon == Icons.keyboard_arrow_down) {
        arrowIcon = Icons.keyboard_arrow_right;
      } else {
        arrowIcon = Icons.keyboard_arrow_down;
      }
    });
  }

  void _onExpansionFError(bool value) {
    setState(() {
      if (arrowIconFError == Icons.keyboard_arrow_down) {
        arrowIconFError = Icons.keyboard_arrow_right;
      } else {
        arrowIconFError = Icons.keyboard_arrow_down;
      }
    });
  }


  //Check image availaibilty
  ImageProvider getImageProvider(File f) {
    return f.existsSync() ? FileImage(f) : const AssetImage("images/user.png");
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            switch (response['calledMethod']) {
              case 'gameWonData':
                // print('gameWonData');
                //print(response['response_data'].toString());
                break;
              case 'playerOne':
                // print(response['response_data'].toString());
                setState(() {
                  player1Name = response['response_data'][0]["name"].toString();
                  playerImage[0] = response['response_data'][0]["player_image"].toString();

                  int x = response['response_data'][0]['Player_no'];
                  if (x < 10) {
                    player1No = "0" + x.toString();
                  } else {
                    player1No = x.toString();
                  }  

                });
                break;
              case 'playerTwo':
                setState(() {
                  player2Name = response['response_data'][0]["name"].toString();
                  playerImage[1] =response['response_data'][0]["player_image"].toString();        

                  int x = response['response_data'][0]['Player_no'];
                  if (x < 10) {
                    player2No = "0" + x.toString();
                  } else {
                    player2No = x.toString();
                  }

                });
                break;
              case 'gameSettings':
                setState(() {
                    date = response['response_data'][0]["game_date"].toString();
                    date = date.split(" ")[0];
                    int setCount =int.parse(response['response_data'][0]["no_of_sets"]); 
                    advantage = response['response_data'][0]["advantage"];
                    getWinnerCount();

                    for (var i = 0; i < setCount; i++) {
                      setState(() {
                       tabelName[i] = tabelNameContainer[i]; 
                      });
                    }
                    //print(date);
                  });
                break;
              case 'getresultData':
                timeConvert(int.parse(response['response_data'][0]['game_time']));                
                break;
              case 'getScoreData':
                
                List dataList = response['response_data'];
                
                String previousGameWon = 'null';
                String previousPointWon = 'null';
               
                player1set = ["", "", "", "", ""];
                player2set = ["", "", "", "", ""];
                tieBreakOnListPlayer1 = ["", "", "", "", ""];
                tieBreakOnListPlayer2 = ["", "", "", "", ""];
                
                int playerSetCount =0,tieBreakCount=0;
                // print("player1Sets "+dataList[dataList.length -1]['player1Sets'].toString());
                // print("player2Sets "+dataList[dataList.length -1]['player2Sets'].toString());

                if (dataList.length != 0) {
                  points= dataList[dataList.length -1]['player1Sets'].toString()+":"+dataList[dataList.length -1]['player2Sets'].toString();                  
                } else {
                  points= "0:0";                  
                }


                for (var i = 0; i < dataList.length; i++) {
                  var item = dataList[i];

                  //player one details set
                  if (item['won'] == widget.arguments["player1"] && item['type'] != ScoreValues.fault) {
                    if (item['type'] == ScoreValues.smash) {
                      smashWinPoint[0]++;
                      winPoint[0]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.foreHand) {
                      foreHandWinPoint[0]++;
                      winPoint[0]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.forHandVolley) {
                      forHandVolleyWinPoint[0]++;
                      winPoint[0]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.backHand) {
                      backHandWinPoint[0]++;
                      winPoint[0]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.backHandVolley) {
                      backHandVolleyWinPoint[0]++;
                      winPoint[0]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.winner) {
                      winPoint[0]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.ace) {
                      aceCount[0]++;
                      point[0]++;
                     // servicePointWon[0]++;
                    } else if (item['type'] == ScoreValues.fErr) {
                      fErrCount[1]++;
                      point[0]++;
                    }else if (item['type'] == ScoreValues.fErrBackHand) {
                      fErrCount[1]++;
                      fErrBackHand[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.fErrBackHandVolley) {
                      fErrCount[1]++;
                      fErrBackHandVolley[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.fErrForeHand) {
                      fErrCount[1]++;
                      fErrForeHand[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.fErrForHandVolley) {
                      fErrCount[1]++;
                      fErrForHandVolley[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.fErrSmash) {
                      fErrCount[1]++;
                      fErrSmash[1]++;
                      point[0]++;
                    }
                     else if (item['type'] == ScoreValues.uErr) {
                      uErr[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.uErrBackHand) {
                      uErr[1]++;
                      uErrBackHand[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.uErrBackHandVolley) {
                      uErr[1]++;
                      uErrBackHandVolley[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.uErrForeHand) {
                      uErr[1]++;
                      uErrForeHand[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.uErrForHandVolley) {
                      uErr[1]++;
                      uErrForHandVolley[1]++;
                      point[0]++;
                    } else if (item['type'] == ScoreValues.uErrSmash) {
                      uErr[1]++;
                      uErrSmash[1]++;
                      point[0]++;
                    }
                    else if (item['type'] == ScoreValues.dflt) {
                      point[0]++;
                      dFltCount[1]++;
                    }
                    

                    //serve point won calculating
                    if(item['serveChance'] == 1 && item['serve'] == 1 ){
                      servicePointWon[0]++;
                      firstServicePointWon[0]++;
                    }
                    else if(item['serveChance'] == 2 && item['serve'] == 1 ){
                      servicePointWon[0]++;
                      secondServicePointWon[0]++;
                    }

                    //recive point won calculating  
                    if(item['serveChance'] == 1 && item['serve'] == 2 ){
                      reciverPoints[0]++;
                      firstReciverPoints[0]++;
                    }
                    else if(item['serveChance'] == 2 && item['serve'] == 2 ){
                      reciverPoints[0]++;
                      secondReciverPoints[0]++;
                    }                    

                    //service game won
                    if (item['serve'] == 1 && item['gameFinished'] == 1) {
                      serviceGametWon[0]++;
                    }


                    //recive game won
                    if (item['serve'] == 2 && item['gameFinished'] == 1 && item['tieBreakOn'] == 0) {
                      reciveGameWon[0]++;
                    }
                    
                    //breakpoint calculation
                    if(i!= 0){
                      if (item['serve'] == 2){
                        //player one calculation
                        if(item['player1Points'] == 40 && item['player2Points'] < 40){
                          breakPoint[0]++;                        
                        }
                        else if(item['player1Points'] == 41){
                          breakPoint[0]++;
                        }
                        else if(((dataList[i - 1]['player1Points'] == 41 || dataList[i - 1]['player2Points'] == 41) && (item['player1Points'] == 40)) && advantage == ScoreValues.semiAdvantage){
                          breakPoint[0]++;
                        }  
                      }
                    }


                  } 
                  else if (item['won'] == widget.arguments["player2"] && item['type'] != ScoreValues.fault) {
                    //print(item);
                    if (item['type'] == ScoreValues.smash) {
                      winPoint[1]++;
                      smashWinPoint[1]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.foreHand) {
                      winPoint[1]++;
                      foreHandWinPoint[1]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.forHandVolley) {
                      winPoint[1]++;
                      forHandVolleyWinPoint[1]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.backHand) {
                      winPoint[1]++;
                      backHandWinPoint[1]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.backHandVolley) {
                      winPoint[1]++;
                      backHandVolleyWinPoint[1]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.winner) {
                      winPoint[1]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.ace) {
                      aceCount[1]++;
                      point[1]++;
                      //servicePointWon[1]++;
                    } else if (item['type'] == ScoreValues.fErr) {
                      fErrCount[0]++;
                      point[1]++;
                    }else if (item['type'] == ScoreValues.fErrBackHand) {
                      fErrCount[0]++;
                      fErrBackHand[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.fErrBackHandVolley) {
                      fErrCount[0]++;
                      fErrBackHandVolley[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.fErrForeHand) {
                      fErrCount[0]++;
                      fErrForeHand[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.fErrForHandVolley) {
                      fErrCount[0]++;
                      fErrForHandVolley[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.fErrSmash) {
                      fErrCount[0]++;
                      fErrSmash[0]++;
                      point[1]++;
                    }                  
                    else if (item['type'] == ScoreValues.uErr) {
                      uErr[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.uErrBackHand) {
                      uErr[0]++;
                      uErrBackHand[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.uErrBackHandVolley) {
                      uErr[0]++;
                      uErrBackHandVolley[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.uErrForeHand) {
                      uErr[0]++;
                      uErrForeHand[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.uErrForHandVolley) {
                      uErr[0]++;
                      uErrForHandVolley[0]++;
                      point[1]++;
                    } else if (item['type'] == ScoreValues.uErrSmash) {
                      uErr[0]++;
                      uErrSmash[0]++;
                      point[1]++;
                    }else if(item['type'] == ScoreValues.dflt){
                      point[1]++;
                      dFltCount[0]++;

                    }

                    //serve point won calculating                   
                    if(item['serveChance'] == 1 && item['serve'] == 2 ){
                      servicePointWon[1]++;
                      firstServicePointWon[1]++;
                    }
                    else if(item['serveChance'] == 2 && item['serve'] == 2 ){
                      servicePointWon[1]++;
                      secondServicePointWon[1]++;
                    }

                    //recive point won calculating                  
                    if(item['serveChance'] == 1 && item['serve'] == 1 ){
                      reciverPoints[1]++;
                      firstReciverPoints[1]++;
                    }
                    else if(item['serveChance'] == 2 && item['serve'] == 1 ){
                      reciverPoints[1]++;
                      secondReciverPoints[1]++;
                    }
                    
                    //service game won
                    if (item['serve'] == 2 && item['gameFinished'] == 1) {
                      serviceGametWon[1]++;
                    }

                    //recive game won
                    if (item['serve'] == 1 && item['gameFinished'] == 1 && item['tieBreakOn'] == 0) {
                      reciveGameWon[1]++;
                    }

                    //breakpoint calculation
                    if (i!= 0) {
                      //player 2 calculation
                      if(item['serve'] == 1){
                        if(item['player2Points'] == 40 && item['player1Points'] < 40){
                          breakPoint[1]++;                        
                        }
                        else if(item['player2Points'] == 41 ){
                          breakPoint[1]++;
                        }
                        else if(((dataList[i - 1]['player1Points'] == 41 || dataList[i - 1]['player2Points'] == 41) && (item['player2Points'] == 40)) && advantage == ScoreValues.semiAdvantage){                       
                          breakPoint[1]++;
                        }              
                      }
                    }

                  }
                  
                  if (item['type'] != ScoreValues.fault ) {
                    
                    //table display -- set point won 
                    if (item['setFinished'] == 1) {
                      player1set[playerSetCount] = item['player1Games'].toString();
                      player2set[playerSetCount] = item['player2Games'].toString();
                      playerSetCount++;                    
                    }

                    //table display -- tie break point won 
                    if (item['setFinished'] == 1 && item['tieBreakOn'] != 0) {
                      // print(item);
                      tieBreakCount = item['totalSets'];
                      --tieBreakCount;
                      tieBreakOnListPlayer1[tieBreakCount] = item['player1Points'].toString();
                      tieBreakOnListPlayer2[tieBreakCount] = item['player2Points'].toString();
                    }
                  }

                  //fault values
                  if (item['serve'] == 1 && item['type'] == ScoreValues.fault) {
                    fltCount[0]++;
                  }
                  if (item['serve'] == 2 && item['type'] == ScoreValues.fault) {
                    fltCount[1]++;
                  }

                  if (item["gameFinished"] == 1) {
                    if (previousGameWon == item['won']) {
                      if (item['won'] == widget.arguments["player1"]) {
                        maxGameWonInRow[0]++;
                      } else {
                        maxGameWonInRow[1]++;
                      }
                    } else {
                      previousGameWon = item['won'];
                    }
                  }

                  if (previousPointWon == item['won']) {
                    if (item['won'] == widget.arguments["player1"]) {
                      maxGamePointInRow[0]++;
                    } else {
                      maxGamePointInRow[1]++;
                    }
                  } else {
                    previousPointWon = item['won'];
                  }
              
                }

                //make zero when their is no won in ROW or won in ROW matches
                if (maxGamePointInRow[0] == 1) {
                  maxGamePointInRow[0] = 0;
                }
                if (maxGamePointInRow[1] == 1) {
                  maxGamePointInRow[1] = 0;
                }
                if (maxGameWonInRow[0] == 1) {
                  maxGameWonInRow[0] = 0;
                }
                if (maxGameWonInRow[1] == 1) {
                  maxGameWonInRow[1] = 0;
                }

                if ((point[0] + point[1]) == 0) {
                  //when win points are 0
                  smashWinPointString[0] = "0";
                  foreHandWinPointString[0] = "0";
                  forHandVolleyWinPointString[0] = "0";
                  backHandWinPointString[0] = "0";
                  backHandVolleyWinPointString[0] = "0";

                  uErrBackHandString[0] = "0";
                  uErrBackHandVolleyString[0] = "0";
                  uErrForeHandString[0] = "0";
                  uErrForHandVolleyString[0] = "0";
                  uErrSmashString[0] = "0";

                  fErrBackHandString[0] = "0";
                  fErrBackHandVolleyString[0] = "0";
                  fErrForeHandString[0] = "0";
                  fErrForHandVolleyString[0] = "0";
                  fErrSmashString[0] = "0";

                  smashWinPointString[1] = "0";
                  foreHandWinPointString[1] = "0";
                  forHandVolleyWinPointString[1] = "0";
                  backHandWinPointString[1] = "0";
                  backHandVolleyWinPointString[1] = "0";

                  uErrBackHandString[1] = "0";
                  uErrBackHandVolleyString[1] = "0";
                  uErrForeHandString[1] = "0";
                  uErrForHandVolleyString[1] = "0";
                  uErrSmashString[1] = "0";

                  fErrBackHandString[1] = "0";
                  fErrBackHandVolleyString[1] = "0";
                  fErrForeHandString[1] = "0";
                  fErrForHandVolleyString[1] = "0";
                  fErrSmashString[1] = "0";
                } else {
                  //player one calculation
                  smashWinPointString[0] =(smashWinPoint[0] / (point[0] + point[1]) * 100).round().toString();
                  foreHandWinPointString[0] =(foreHandWinPoint[0] / (point[0] + point[1]) * 100).round().toString();
                  forHandVolleyWinPointString[0] = (forHandVolleyWinPoint[0] /(point[0] + point[1]) *100).round().toString();
                  backHandWinPointString[0] =(backHandWinPoint[0] / (point[0] + point[1]) * 100).round().toString();
                  backHandVolleyWinPointString[0] = (backHandVolleyWinPoint[0] /(point[0] + point[1]) *100).round().toString();

                  uErrBackHandString[0] =(uErrBackHand[0] / (point[0] + point[1]) * 100).round().toString();
                  uErrBackHandVolleyString[0] = (uErrBackHandVolley[0] /(point[0] + point[1]) *100).round().toString();
                  uErrForeHandString[0] =(uErrForeHand[0] / (point[0] + point[1]) * 100).round().toString();
                  uErrForHandVolleyString[0] =(uErrForHandVolley[0] / (point[0] + point[1]) * 100).round().toString();
                  uErrSmashString[0] =(uErrSmash[0] / (point[0] + point[1]) * 100).round().toString();

                  fErrBackHandString[0] =(fErrBackHand[0] / (point[0] + point[1]) * 100).round().toString();
                  fErrBackHandVolleyString[0] = (fErrBackHandVolley[0] /(point[0] + point[1]) *100).round().toString();
                  fErrForeHandString[0] =(fErrForeHand[0] / (point[0] + point[1]) * 100).round().toString();
                  fErrForHandVolleyString[0] =(fErrForHandVolley[0] / (point[0] + point[1]) * 100).round().toString();
                  fErrSmashString[0] =(fErrSmash[0] / (point[0] + point[1]) * 100).round().toString();


                  //player two calculation
                  smashWinPointString[1] =(smashWinPoint[1] / (point[0] + point[1]) * 100).round().toString();
                  foreHandWinPointString[1] =(foreHandWinPoint[1] / (point[0] + point[1]) * 100).round().toString();
                  forHandVolleyWinPointString[1] = (forHandVolleyWinPoint[1] /(point[0] + point[1]) *100).round().toString();
                  backHandWinPointString[1] =(backHandWinPoint[1] / (point[0] + point[1]) * 100).round().toString();
                  backHandVolleyWinPointString[1] = (backHandVolleyWinPoint[1] /(point[0] + point[1]) *100).round().toString();

                  uErrBackHandString[1] =(uErrBackHand[1] / (point[0] + point[1]) * 100).round().toString();
                  uErrBackHandVolleyString[1] = (uErrBackHandVolley[1] /(point[0] + point[1]) *100).round().toString();
                  uErrForeHandString[1] =(uErrForeHand[1] / (point[0] + point[1]) * 100).round().toString();
                  uErrForHandVolleyString[1] =(uErrForHandVolley[1] / (point[0] + point[1]) * 100).round().toString();
                  uErrSmashString[1] =(uErrSmash[1] / (point[0] + point[1]) * 100).round().toString();

                  fErrBackHandString[1] =(fErrBackHand[1] / (point[0] + point[1]) * 100).round().toString();
                  fErrBackHandVolleyString[1] = (fErrBackHandVolley[1] /(point[0] + point[1]) *100).round().toString();
                  fErrForeHandString[1] =(fErrForeHand[1] / (point[0] + point[1]) * 100).round().toString();
                  fErrForHandVolleyString[1] =(fErrForHandVolley[1] / (point[0] + point[1]) * 100).round().toString();
                  fErrSmashString[1] =(fErrSmash[1] / (point[0] + point[1]) * 100).round().toString();
                }

                setState(() {
                  //player one data set
                  smashWinPointString[0] = smashWinPoint[0].toString() +"[" +smashWinPointString[0] +"%]";
                  foreHandWinPointString[0] = foreHandWinPoint[0].toString() +"[" +foreHandWinPointString[0] +"%]";
                  forHandVolleyWinPointString[0] =forHandVolleyWinPoint[0].toString() +"[" +forHandVolleyWinPointString[0] +"%]";
                  backHandWinPointString[0] = backHandWinPoint[0].toString() +"[" +backHandWinPointString[0] +"%]";
                  backHandVolleyWinPointString[0] =backHandVolleyWinPoint[0].toString() +"[" +backHandVolleyWinPointString[0] +"%]";
                  
                  uErrBackHandString[0] = uErrBackHand[0].toString() +"[" +uErrBackHandString[0] +"%]";
                  uErrBackHandVolleyString[0] =uErrBackHandVolley[0].toString() +"[" +uErrBackHandVolleyString[0] +"%]";
                  uErrForeHandString[0] = uErrForeHand[0].toString() +"[" +uErrForeHandString[0] +"%]";
                  uErrForHandVolleyString[0] = uErrForHandVolley[0].toString() +"[" +uErrForHandVolleyString[0] +"%]";
                  uErrSmashString[0] =uErrSmash[0].toString() + "[" + uErrSmashString[0] + "%]";

                  fErrBackHandString[0] = fErrBackHand[0].toString() +"[" +fErrBackHandString[0] +"%]";
                  fErrBackHandVolleyString[0] =fErrBackHandVolley[0].toString() +"[" +fErrBackHandVolleyString[0] +"%]";
                  fErrForeHandString[0] = fErrForeHand[0].toString() +"[" +fErrForeHandString[0] +"%]";
                  fErrForHandVolleyString[0] = fErrForHandVolley[0].toString() +"[" +fErrForHandVolleyString[0] +"%]";
                  fErrSmashString[0] =fErrSmash[0].toString() + "[" + fErrSmashString[0] + "%]";
                  
                  //palyer one first serive point won                  
                  if ((firstServicePointWon[0] + firstReciverPoints[1]) != 0) {
                    firstServicePointWonString[0] =(((firstServicePointWon[0] / (firstServicePointWon[0] + firstReciverPoints[1])) * 100).round()).toString() +"%";
                    firstServicePointWonString[0] = "["+firstServicePointWon[0].toString()+"/"+(firstServicePointWon[0] + firstReciverPoints[1]).toString()+"]"+ firstServicePointWonString[0];                    

                  } else {
                    firstServicePointWonString[0] = "["+firstServicePointWon[0].toString()+"/"+(firstServicePointWon[0] + firstReciverPoints[1]).toString()+"]0%";                    
                  }

                  //palyer one first recive point won
                  if ((firstReciverPoints[0] + firstServicePointWon[1]) != 0 ) {
                    firstReciverPointsString[0] =(((firstReciverPoints[0] / (firstReciverPoints[0] + firstServicePointWon[1])) * 100).round()).toString() +"%";
                    firstReciverPointsString[0] = "["+firstReciverPoints[0].toString()+"/"+(firstReciverPoints[0] + firstServicePointWon[1]).toString()+"]"+ firstReciverPointsString[0];                    
                    
                  } else {
                    firstReciverPointsString[0] = "["+firstReciverPoints[0].toString()+"/"+(firstReciverPoints[0] + firstServicePointWon[1]).toString()+"]0%";                    
                  }

                  //palyer one second serive point won                  
                  if ((secondServicePointWon[0] + secondReciverPoints[1]) != 0) {
                    secondServicePointWonString[0] =(((secondServicePointWon[0] / (secondServicePointWon[0] + secondReciverPoints[1])) * 100).round()).toString() +"%";
                    secondServicePointWonString[0] = "["+secondServicePointWon[0].toString()+"/"+(secondServicePointWon[0] + secondReciverPoints[1]).toString()+"]"+ secondServicePointWonString[0];                    

                  } else {
                    secondServicePointWonString[0] = "["+secondServicePointWon[0].toString()+"/"+(secondServicePointWon[0] + secondReciverPoints[1]).toString()+"]0%";
                  }

                  //palyer one second recive point won                                    
                  if (((secondReciverPoints[0] + secondServicePointWon[1]) != 0)) {
                    secondReciverPointsString[0] =(((secondReciverPoints[0] / (secondReciverPoints[0] + secondServicePointWon[1])) * 100).round()).toString() +"%";
                    secondReciverPointsString[0] = "["+secondReciverPoints[0].toString()+"/"+(secondReciverPoints[0] + secondServicePointWon[1]).toString()+"]"+ secondReciverPointsString[0];                    
                    
                  } else {
                    secondReciverPointsString[0] = "["+secondReciverPoints[0].toString()+"/"+(secondReciverPoints[0] + secondServicePointWon[1]).toString()+"]0%";                    
                  }

                  //player one and two serve In
                  if ((firstReciverPoints[0] + firstServicePointWon[1]) != 0) {  
                    firstServiceInString[0] = ((firstReciverPoints[1] + firstServicePointWon[0])/(firstReciverPoints[1] + firstServicePointWon[0]+fltCount[0])*100).round().toString();
                    firstServiceInString[0] = "["+(firstReciverPoints[1] + firstServicePointWon[0]).toString()+"/"+(firstReciverPoints[1] + firstServicePointWon[0]+fltCount[0]).toString()+"]"+firstServiceInString[0]+"%";

                    firstServiceInString[1] = ((firstReciverPoints[0] + firstServicePointWon[1])/(firstReciverPoints[0] + firstServicePointWon[1]+fltCount[1])*100).round().toString();
                    firstServiceInString[1] = "["+(firstReciverPoints[0] + firstServicePointWon[1]).toString()+"/"+(firstReciverPoints[0] + firstServicePointWon[1]+fltCount[1]).toString()+"]"+firstServiceInString[1]+"%";
                  } else {
                    firstServiceInString[0] = "["+(firstReciverPoints[1] + firstServicePointWon[0]).toString()+"/"+(firstReciverPoints[1] + firstServicePointWon[0]+fltCount[0]).toString()+"]0%";
                    firstServiceInString[1] = "["+(firstReciverPoints[0] + firstServicePointWon[1]).toString()+"/"+(firstReciverPoints[0] + firstServicePointWon[1]+fltCount[1]).toString()+"]0%";

                  }

                  //player two data set
                  smashWinPointString[1] = smashWinPoint[1].toString() +"[" +smashWinPointString[1] +"%]";
                  foreHandWinPointString[1] = foreHandWinPoint[1].toString() +"[" +foreHandWinPointString[1] +"%]";
                  forHandVolleyWinPointString[1] =forHandVolleyWinPoint[1].toString() + "[" +forHandVolleyWinPointString[1] +"%]";
                  backHandWinPointString[1] = backHandWinPoint[1].toString() +"[" +backHandWinPointString[1] +"%]";
                  backHandVolleyWinPointString[1] =backHandVolleyWinPoint[1].toString() +"[" +backHandVolleyWinPointString[1] +"%]";
                  
                  uErrBackHandString[1] = uErrBackHand[1].toString() +"[" +uErrBackHandString[1] +"%]";
                  uErrBackHandVolleyString[1] =uErrBackHandVolley[1].toString() +"[" +uErrBackHandVolleyString[1] +"%]";
                  uErrForeHandString[1] = uErrForeHand[1].toString() +"[" +uErrForeHandString[1] +"%]";
                  uErrForHandVolleyString[1] = uErrForHandVolley[1].toString() +"[" +uErrForHandVolleyString[1] +"%]";
                  uErrSmashString[1] =uErrSmash[1].toString() + "[" + uErrSmashString[1] + "%]";

                  fErrBackHandString[1] = fErrBackHand[1].toString() +"[" +fErrBackHandString[1] +"%]";
                  fErrBackHandVolleyString[1] =fErrBackHandVolley[1].toString() +"[" +fErrBackHandVolleyString[1] +"%]";
                  fErrForeHandString[1] = fErrForeHand[1].toString() +"[" +fErrForeHandString[1] +"%]";
                  fErrForHandVolleyString[1] = fErrForHandVolley[1].toString() +"[" +fErrForHandVolleyString[1] +"%]";
                  fErrSmashString[1] =fErrSmash[1].toString() + "[" + fErrSmashString[1] + "%]";
                  
                  //palyer two first serive point won                  
                  if((firstServicePointWon[1] + firstReciverPoints[0]) != 0){                  
                    firstServicePointWonString[1] =(((firstServicePointWon[1] / (firstServicePointWon[1] + firstReciverPoints[0])) * 100).round()).toString() +"%";
                    firstServicePointWonString[1] = "["+firstServicePointWon[1].toString()+"/"+(firstServicePointWon[1] + firstReciverPoints[0]).toString()+"]"+ firstServicePointWonString[1];
                  }else{                
                    firstServicePointWonString[1] = "["+firstServicePointWon[1].toString()+"/"+(firstServicePointWon[1] + firstReciverPoints[0]).toString()+"]0%";
                  }

                  //palyer two first recive point won                  
                  if ((firstReciverPoints[0] + firstServicePointWon[1]) != 0 ) {
                    firstReciverPointsString[1] =(((firstReciverPoints[1] / (firstReciverPoints[1] + firstServicePointWon[0])) * 100).round()).toString() +"%";
                    firstReciverPointsString[1] = "["+firstReciverPoints[1].toString()+"/"+(firstReciverPoints[1] + firstServicePointWon[0]).toString()+"]"+ firstReciverPointsString[1];                                        
                  } else {
                    firstReciverPointsString[1] = "["+firstReciverPoints[1].toString()+"/"+(firstReciverPoints[1] + firstServicePointWon[0]).toString()+"]0%";                    
                  }

                  //palyer two first service point won                                    
                  if ((secondServicePointWon[1] + secondReciverPoints[0]) != 0 ) {
                    secondServicePointWonString[1] =(((secondServicePointWon[1] / (secondServicePointWon[1] + secondReciverPoints[0])) * 100).round()).toString() +"%";
                    secondServicePointWonString[1] = "["+secondServicePointWon[1].toString()+"/"+(secondServicePointWon[1] + secondReciverPoints[0]).toString()+"]"+ secondServicePointWonString[1];                      
                  } else {
                    secondServicePointWonString[1] = "["+secondServicePointWon[1].toString()+"/"+(secondServicePointWon[1] + secondReciverPoints[0]).toString()+"]0%";
                  }

                  //palyer two second recive point won                                    
                  if ((secondReciverPoints[1] + secondServicePointWon[0]) != 0) {
                    secondReciverPointsString[1] =(((secondReciverPoints[1] / (secondReciverPoints[1] + secondServicePointWon[0])) * 100).round()).toString() +"%";
                    secondReciverPointsString[1] = "["+secondReciverPoints[1].toString()+"/"+(secondReciverPoints[1] + secondServicePointWon[0]).toString()+"]"+ secondReciverPointsString[1];                    
                    
                  } else {
                    secondReciverPointsString[1] = "["+secondReciverPoints[1].toString()+"/"+(secondReciverPoints[1] + secondServicePointWon[0]).toString()+"]0%";                    
                  }

                });
                caluculateGameWinPint();
                timeBreakValues();
                break;
              case 'playerTwoGetWinnerCount':
                break;

              default:
            }
          } else {}
          break;
        }
      default:
        {}
    }
  }
}
