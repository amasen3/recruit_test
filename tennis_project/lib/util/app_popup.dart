import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/util/app_const.dart';

class AppPopup extends StatelessWidget {
  final BuildContext context;
  final String optionType;
  final List<String> param;
  final Function callBackFunction;
  AppPopup(this.context, this.optionType, this.param, this.callBackFunction);

  @override
  Widget build(BuildContext context) {
    String firstButtonText = '';
    String secondButtonText = 'Cancel';
    String topicText = '';
    var textDesplay = "";
    var iconDesplay;

    if (optionType == ScoreValues.pause) {
      textDesplay = 'Paused';
      topicText = 'PAUSE';
      firstButtonText = "OK";
      iconDesplay = Icons.pause;
    }

    if (optionType == ScoreValues.stop) {
      textDesplay = 'Stopped';
      topicText = 'STOP';
      firstButtonText = "OK";
      iconDesplay = Icons.stop;
    }

    if (optionType == ScoreValues.delete) {
      textDesplay = 'Deleted';
      topicText = 'Delete';
      firstButtonText = "OK";
      iconDesplay = Icons.delete;
    }

    return AlertDialog(
      title: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Icon(iconDesplay, size: 30, color: Colors.black),
              ],
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: new Text(
                    topicText,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 25,
                        color: Colors.black,
                        fontStyle: FontStyle.normal),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      content: new Text(
          "This Game will be " + textDesplay + ". Do you want to continue ?",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
              color: Colors.black,
              fontStyle: FontStyle.normal)),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 20.0, bottom: 10.0),
          child: Container(
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(
                      width: 100,
                      child: new RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            side: BorderSide(
                                color: AppStyle.color_Head,
                                style: BorderStyle.solid)),
                        color: AppStyle.color_Head,
                        child: new Text(
                          firstButtonText,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: Colors.white,
                              fontStyle: FontStyle.normal),
                        ),
                        onPressed: () => {
                          Navigator.of(context).pop(),
                          callBackFunction(param)
                        },
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: SizedBox(
                        width: 100,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              side: BorderSide(
                                  color: AppStyle.color_Head,
                                  style: BorderStyle.solid)),
                          color: AppStyle.color_Head,
                          child: new Text(
                            secondButtonText,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                fontStyle: FontStyle.normal),
                          ),
                          onPressed: () => {
                            Navigator.of(context).pop(),
                            callBackFunction(["cancel", optionType])
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
