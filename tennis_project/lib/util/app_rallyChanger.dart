import 'package:flutter/material.dart';

class RallyChanger extends StatefulWidget {
  Function onIncremented;
  Function onDecremented;
  int dropdownRallyCount;

  RallyChanger(
      {this.dropdownRallyCount, this.onIncremented, this.onDecremented});

  @override
  RallyChangerState createState() => RallyChangerState();
}

class RallyChangerState extends State<RallyChanger> {
  int dropdownRallyCount = 0;
  @override
  void initState() {
    super.initState();
    dropdownRallyCount = widget.dropdownRallyCount;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FloatingActionButton(
              onPressed: () {
                setState(() {
                  dropdownRallyCount++;
                });
                widget.onIncremented(dropdownRallyCount);
              },
              child: Icon(
                Icons.add,
                color: Colors.black,
              ),
              backgroundColor: Colors.white,
            ),
            Text('$dropdownRallyCount', style: new TextStyle(fontSize: 16.0)),
            FloatingActionButton(
              onPressed: () {
                if (dropdownRallyCount != 1) {
                  setState(() {
                    dropdownRallyCount--;
                  });
                }
                widget.onDecremented(dropdownRallyCount);
              },
              child: Icon(const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                  color: Colors.black),
              backgroundColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
